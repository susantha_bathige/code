

# sql server with agent/ no shared volume
docker run -e MSSQL_PID=Developer -e ACCEPT_EULA=Y -e SA_PASSWORD=12qwaszx@ -e MSSQL_AGENT_ENABLED=true -h sql2019 --name sql2019 -p 1433:1433 -d mcr.microsoft.com/mssql/server:2019-latest

# sql server with docker volume
docker run -e MSSQL_PID=Developer -e ACCEPT_EULA=Y -e SA_PASSWORD=12qwaszx@ -v D:\SqlData_Docker/data:/var/opt/mssql/data -v D:\SqlData_Docker/log:/var/opt/mssql/log -v D:\SqlData_Docker/backup:/tmp -e MSSQL_AGENT_ENABLED=true -h sql2019 --name sql2019 -p 1433:1433 -d mcr.microsoft.com/mssql/server:2019-latest

docker run -e MSSQL_PID=Developer -e ACCEPT_EULA=Y -e SA_PASSWORD='12qwaszx@' -v \\wsl$\Ubuntu-20.04\home\susanthab\sql-server/data:/tmp/data -v \\wsl$\Ubuntu-20.04\home\susanthab\sql-server/log:/tmp/log -v \\wsl$\Ubuntu-20.04\home\susanthab\sql-server/backup:/tmp/backup -e MSSQL_AGENT_ENABLED=true -h sql2019 --name sql2019 -p 1433:1433 -d mcr.microsoft.com/mssql/server:2019-latest

docker run -e MSSQL_PID=Developer -e ACCEPT_EULA=Y -e SA_PASSWORD='12qwaszx@' -v \\wsl$\Ubuntu-20.04\home\susanthab\sql-server/data:/mssql/data -v \\wsl$\Ubuntu-20.04\home\susanthab\sql-server/log:/mssql/log -v \\wsl$\Ubuntu-20.04\home\susanthab\sql-server/backup:/mssql/backup -e MSSQL_AGENT_ENABLED=true -h sql2019 --name sql2019 -p 1433:1433 -d mcr.microsoft.com/mssql/server:2019-latest


# remove all unused images
docker image prune -a

# Restore AdventureWorks db
# USE [master]
# RESTORE DATABASE [AdventureWorks2017] FROM  DISK = N'/var/opt/mssql/data/AdventureWorks2017.bak' 
# WITH  FILE = 1,  MOVE N'AdventureWorks2017' TO N'/var/opt/mssql/data/AdventureWorks2017.mdf',  
# 				 MOVE N'AdventureWorks2017_log' TO N'/var/opt/mssql/data/AdventureWorks2017_log.ldf',  
# 				 NOUNLOAD,  STATS = 5

# GO


## flyway
docker run --rm -v D:/flyway/sql:/flyway/sql -v D:/flyway/conf:/flyway/conf flyway/flyway

## conf file
flyway.url=jdbc:sqlserver://192.168.0.3:1433;databaseName=TestDBMigration
flyway.user=sa
flyway.password=password
flyway.schemas=dbo
flyway.baselineOnMigrate=true
flyway.locations=filesystem:/flyway/sql
flyway.baselineVersion=1