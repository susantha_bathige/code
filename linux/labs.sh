<#
Permissions function differently when applied to directories. Read allows users to view the contents of a directory, 
write allows users to add and delete files, and execute allows users to cd into that directory.
#>

<#
The find command is highly versatile, but in this case you will be using it to locate files that have permissions that may pose security risks.
#>

find -perm 755

# any files which has perm 755 or greater
find -perm -755

# The {} \; at the end of the command is telling find to execute the chmod command separately on each file that matches the search criteria.
find -perm -755 -type f -exec chmod 600 {} \;


