# --------------------
# Basic Linux commands
# --------------------
# ls - list dir contents. 
#   -l long listing
# cd - change the current dir. 
# pwd - displays the present working dir. 
# cat - Concatenates and display files. 
# echo - Displays arguments to the screen. 
# man - Displays the online manual. 
# exit - Exists the shell or current session. 
# clear - Clear the screen. 

# Using man command
# space bar - to move pages
# g - go to top
# G - go to bottom

# locate a command. 
which cat

# displays the file contents in reverse order
tac 

# Searching man pages
man -k SEARCH_TERM

# Directory shortcuts
# change to the previous dir
cd - 

# dir separator
/

ls -l
# drwx------    7 ubathsu  PEROOT\Domain Users      238 Aug 30 10:43 Applications
# permissions               - drwx------
# Number of links           - 7 
# Owner name                - ubathsu
# Group name                - PEROOT\Domain Users
# No.of bytes in the file   - 238
# Last mod time             - Aug 30 10:43
# File name                 - Applications

# to see hiden files
ls -a 

# to reveal file types
ls -F 
# / -> dir
# @ -> Link
# * -> Executable

# list files by time
ls -t
#  -r reverse order
#  -latr Long listing including all files, reverse sorted by time 
#  -R list files recursively
#  -d List dir names, not contents
#  --color Colorize the output

# Working with Spaces in Names
# It is always better to avoid spaces in file names and dir names. 
# Use a backslash (\) to escape spaces. 


#------------------------------------------
# File and Directory Permissions explained. 
#------------------------------------------
drwx------

# Symbol                Type
#   -                   Regular file
#   d                   Dir
#   l                   Symbolic link
#   r                   Read (you can see its contents) (4)
#   w                   Write  (2)
#   x                   Execute (1)

# Permission categories

#   Symbol              Category
#     u                 User
#     g                 Group
#     o                 Other   
#     a                 All

# displays a user's group
groups
id -Gn # gives the same output

# Manage permissions using chmod
# chmod command has the following format. 

# Item          Meaning
#  chmod        Change mode command
#  ugoa         User(u)m group(g), or(o) other , all(a)
#  +-=          Add(+), substract(-), set (=)
#  rwx          Read(r), write(w), execute(x)

# Add write permission to sales.data group.
chmod g+w sales.data

# Add r,w,and x permission to user and minus x for the group. 
chmod u+rwx, g-x sales.data

# Set all to r (user, group and other)
chmod a=r sales.data

# In-addition to symbolic permission for chmod, a numeric based permissions also can be used. 

#   r    w   x
#   0    0   0    Value of off
#   1    1   1    Binary value for on
#   4    2   1    Based 10 value for on

# In permission order has a meaning. 

locate <filename>
# updatedb


# In permission order has a meaning. 