# Get EC2 instance IP (external)
dig -x $(curl -s checkip.amazonaws.com) +short

# Get Host IP address of EC2 (internal)
nslookup $HOSTNAME | grep -A1 'Name:' | grep 'Address:' | grep -o " [^ ]*$"

# Free mem
free -mh

# disk related
fdisk -l
df -Th
df -h

# *******
# Network
# *******

# list all network cards (IP add show)
ip a s

# list net connections
nmcli conn show

# conn up
nmcli conn up eth03s

# change ONBOOT
sed -i s/ONBOOT=no/ONBOOT=yes/ /etc/sysconfig/network-scripts/ifcfg-enp0s3
grep ONBOOT !$ # !$ means last argument. 

# ************
# Gerenal tips
# ************
#   wheel group is the admin group in Linux


# *******
# Updates
# *******

yum update
yup install -y pkg1, pkg2, etc
yum groupinstall -y "Development Tools"
yum groupinstall -y "X Window System"
yum list installed # list installed packages.  

# ************
# Service mgmt
# ************

systemctl 

# *************************
# Setting Graphical Desktop
# *************************

systemctl set-default graphical.target
systemctl isolate graphical.target

# *****
# users
# *****

cat /etc/passwd
who
CTRL + L # clear the screen. 
pwd # full path to the home dir. 
ls # list contents
type ls
ls -a # all including the hidden files. 
# blue color indicates the dir. 
ls -aF # dir will have / at the end. 
# File type ends with @ are symbolic links. 
ls -l # to get long listing. permission, file size, ownership, etc. 
ls -lrt # to get most recentlt modified files. r=reverse, t=time
# most recently editted file in the bottom. 
ls -lhrt # h=human readable format. 
ls -ld /etc # list the etc dir itself rather the contents of it. 
# drwxr-xr-x. 112 root root 8.0K Jul 23 15:23 /etc/
#   d -> file type (everything in Linux is some form of file.)
#   c -> character device
#   b -> block device
#   l -> links
#   - -> regular file
#   p -> named pipes
#   s -> sockets (open conn)
# rwxr-xr-x. -> permission block
# 112 -> no.of hard links we have for this dir.
# root root -> ownership (user | group)
# 8.0K -> file size. 
# Jul 23 15:23 -> last modified date/time. 
lsb # block devices. 
ls -l /dev/sda? # sda followed by any single char. 
ls -l /dev/sda[12] # sda followed by 1 or 2. 
ls -l /etc/system-release /etc/redhat-release /etc/centos-release
lsb_release -d # get the OS release -d -> desc.
#  lsb_release is a binary file. (executable file)
cat /etc/system-release # get the OS release
ls -l $(which lsb_release)
ls -lF $(which lsb_release) # * at the end of the result means, it is an executable file.

# In centos, it uses the pkg manager as "rpm"
rpm -qf /usr/bin/lsb_release # which pakage contains the lsb_release file. 
#   qf -> query file. Because all the files are stored in the database. 
# shortcut method 
rpm -qf $(which lsb_release)

# -i option will prompt whether to overide the file of passwd exists in the current dir. 
cp -i /etc/hosts ./passwd
#   -i -> interactive mode. 

# ******************
# File mgmt commands
# ******************

mv # move or remove. Again you can use -i mode. 
rm -i * # delete with interactive mode. 
mkdir -p sales/test # create sales parent dir first. 
!rm # last command executed that began with rm.
rm -rf sales # recursive and force delete. 
mkdir one two # creates two dir at the same level. 
touch one/file{1..5} # creates 5 files. 
cp -R one two # recursively cp all the contents of one to two. 
mkdir -m 777 d1 # create a file with the permission
ls -ldi /etc/ # list /etc dir metadata with i node num. 
## 4194369 drwxr-xr-x. 112 root root 8192 Jul 24 06:22 /etc/
# when you create a dir it automatically creates . (current dir) and .. (parent dir) dir as well. 
ln f1 f2 # creates a hard link. Both f1 and f2 files are linked to same metedata. 
ls -li f1 f2 
ln -s f1 f3 # symbolink link (-s)

# **************
# Reading Files. 
# **************

# cat | less | head | tail
#   cat -> for small files
#   less -> for larger files
echo $SSH_CONNECTION # verify ssh conn
cat <file1> <file2>
wc -l /etc/services # word count (wc) of a long file. -l -> lines
less /etc/services # browse thru the file. /string -> searching forward, ?string -> searching backwords. 
#   q -> quit
head -n 3 /etc/services # to see top 3 files. 
tail -n 3 /etc/services # bottom 3 files. 

# Search on files
grep kernel # lines which contains "kernel"
grep ^kernel # begins with "kernel"
grep '\bserver\b' ntp.conf  # searching the word "server"
grep -E 'ion$' /usr/share/dict/words # word ends with "ion"
grep -E '^po..ute$' /usr/share/dict/words # begins with "po" and end with "ute" and any two chars in the middle. 
grep -E '[aeiou]{5}' /usr/share/dict/words # lines with 5 vovals in a row. 

# editing files with SED.
# use various type of regex with sed. 

# Comparing files.
diff <file1> <file2>
# binary files. 
rpm -V ntp # verify a package. 
# get check sum of a binary file. 
md5sum /usr/bin/passwd

# searching files. 
# -delete
find /usr/share/doc -name '*.pdf' -print
find /usr/share/doc -name '*.pdf' -exec cp {} . \;

# look for larger files. 
find /boot -size +20000k -type f

# disk usage
du -sh *

# disk utilization
yum install ncdu
ncdu /

###Network related

#To list all open ports or currently running ports including TCP and UDP
netstat -lntu

# To display open ports and established TCP connections, enter
netstat -vatn

# all the conn
netstat -nat

# 
netstat -ton | grep CLOSE_WAIT

# from which program
netstat -tonp
netstat -A inet -p

netstat -nat | grep ESTAB | grep -v "`cat kubehost.txt`" | wc -l
netstat -nat | grep ESTAB | wc -l
ls -ltr /opt/couchbase/var/lib/couchbase/logs/


#### VIM Shortcuts

# Search and replace
:%s/old_string/new_string/g

# to delete all lines.
:1,$d


# Suddenly I received "Permission denied" error when trying to SSH over tunnelling. 
ssh-add -l

# Nothing was there. 
# Then I added private key to the ssh-agent and everything works. 
ssh-add -K ~/.ssh/id_rsa

# find files over 100MB
find / -xdev -type f -size +100M

# high cpu contender
ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%cpu | head

# check space utilization with sort
du -h --max-depth=1 /var/lib/docker | sort -n
du -h --max-depth=1 /var/ | sort -n

# truncate container log. 
> path.log

# find /var/lib/docker/ -name \*.log -size +1G

# memory troubleshooting. 
grep -i -r 'out of memory' /var/log/

# copy file from my local Windows machine to remote CentOS
scp C:\Users\susantha.bathige\Downloads\dvdrental\dvdrental.tar susantha.bathige@asc-dev-sql01:/temp/

# CentOS 8
# change hostname
vi /etc/hostname

# to list all interfaces
ip addr


# assign a static  ip
# edit this file: cat /etc/sysconfig/network-scrIPts/ifcfg-enp1s0
# change BOOTPROTO = "none"
#        IPADDR=192.168.0.24
#        DNS=205.171.3.25,205.171.2.25

#  to make our changes to effect, you must put down and up again the network interface
sudo nmcli connection down enp1s0 && sudo nmcli connection up enp1s0

# install postgres 12 on CentOS 8
https://www.postgresql.org/download/linux/redhat/

dnf install https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
dnf -qy module disable postgresql
dnf install postgresql12-server
/usr/pgsql-12/bin/postgresql-12-setup initdb --waldir=/pg_wal --pgdata=/pg_data
# /usr/pgsql-12/bin/postgresql-12-setup initdb -D /pg_data
systemctl enable postgresql-12
systemctl start postgresql-12


# setup replication
https://www.tecmint.com/configure-postgresql-streaming-replication-in-centos-8/

# add postgres to the firewall
sudo firewall-cmd --add-service=postgresql --permanent

# https://pgtune.leopard.in.ua/#/


## Admin queries
# if we see high num of idel-in-transaction queries, it is definitely
# important to dig deeper
SELECT datname, count(*) AS open, 
    count(*) FILTER (WHERE state = 'active') AS active, 
    count(*) FILTER (WHERE state = 'idle') AS idle, 
    count(*) FILTER (WHERE state = 'idle in transaction') AS idle_in_trans 
FROM pg_stat_activity 
WHERE backend_type = 'client backend' 
GROUP BY ROLLUP( 1);

# Open transactions
# The VACUUM cmd can only clean up dead rows if o transaction can see it anymore. 
SELECT pid, xact_start, now() - xact_start AS duration 
FROM pg_stat_activity 
WHERE state LIKE '% transaction%' ORDER BY 3 DESC;

# long running queries. 
SELECT now() - query_start AS duration, datname, 
    query 
FROM pg_stat_activity 
WHERE state = 'active' ORDER BY 1 DESC;

# Things we neeed to change in prod system
# show track_activity_query_size; -- 32,768
# ask dev to set application_name variable. This can be set as part of the  query string. 
show application_name 
# finding slow queries
show log_min_duration_statement
alter database test set log_min_duration_statement to 10000;


# missing indexes
# try finding large tables with high avg values
select schemaname, relname, seq_scan, seq_tup_read,
idx_scan, seq_tup_read / seq_scan as avg
from pg_stat_user_tables
where seq_scan > 0
order by seq_tup_read desc
limit 20;

# set the following settings
show work_mem
show maintenance_work_mem

# Linux cmds
vmstat
iostat

# tail equivalent in less
less 
shift+F


## CentOS version
cat /etc/centos-release

## Search and remove rmp package from CentOS 8
rpm -qa | grep -i epel-release
rpm -e <output from the above cmd>

## Remote SSH
## ---------------------------------------------------------------------------------------
!/bin/bash

list=(
  "10.1.39.124"
  "10.1.39.167"
  "10.1.43.52"
  "10.1.51.178"
  "10.1.53.65"
)

for i in "${list[@]}"; do
  echo -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  echo ssh -oStrictHostKeyChecking=no -Att -l susanthab 18.218.95.24 ssh -oStrictHostKeyChecking=no -Att -l susanthab $i sudo netstat -nat
  echo -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  count=$(ssh -oStrictHostKeyChecking=no -Att -l susanthab 18.218.95.24 ssh -oStrictHostKeyChecking=no -Att -l susanthab $i sudo netstat -nat | grep ESTAB | wc -l)
  echo $i has $count established connections
done
## ---------------------------------------------------------------------------------------


## port assignements
cat /etc/services 

## check logs
# live 
sudo journalctl -fu pgbouncer

# with no live
sudo journalctl -u pgbouncer
# SHIFT + g to tail
# https://www.digitalocean.com/community/tutorials/how-to-use-journalctl-to-view-and-manipulate-systemd-logs
sudo journalctl -u pgbouncer --since "now" -f
sudo journalctl -u pgbouncer --since "today" --until "15:50"

# truncate log file
sudo -u enterprisedb truncate -s 1 /var/log/edb/as13/postgresql-Mon.log

# find large files
du -a /var/log/edb/as13/ | sort -n -r | head -n 20
du -a /backups/datadomain/ | sort -n -r | head -n 20
du -sh /backups/datadomain_psql — # the -s option will give us the total size of a specified folder

# create tar file
tar -czvf sample.gz sample.log

# extract the archive
tar -xzvf ample.gz 

# find files
locate <filename>
# updatedb

# find file contentents
grep -r "Hello World" /var/log
# display 1 line before and after the find criteria
grep -C1 "fact" quote.txt

# sort processes by memory usage
ps au --sort=-%mem

# kill 
# kill of the process id of top command
kill -9 $(pidof top)





