## TIPS

# find wifi password
security find-generic-password -wa "dbtorque"

# copy output of a cmd use pbcopy
security find-generic-password -wa "dbtorque" | pbcopy

# Copy something with no formatting
CMD + OPTION + SHIFT + V

# Copy to a clipboard
CMD + CONTROL + SHIFT + 4

# change the location of screencapture
defaults write com.apple.screencapture location ~/Documents/screencapture

# change the default type
defaults write com.apple.screencapture type jpg

# find out what is the shell it is using
which $SHELL

# switch to bash shell
bash
# switch back to zsh
zsh

# set a timer in terminal 
leave 1245

# web server
python3 -m http.server
localhost:8000

# use touchid instead of sudo
sudo vim /etc/pam.d/sudo
auth sufficient pam_tid.so




