

##-------------------------------------------------------------
## MY LAB
##-------------------------------------------------------------
# Install the repository configuration
sudo dnf -y install https://yum.enterprisedb.com/edbrepos/edb-repo-latest.noarch.rpm

# Replace 'USERNAME:PASSWORD' below with your username and password for the EDB repositories
# Visit https://www.enterprisedb.com/user to get your username and password
sudo sed -i "s@<username>:<password>@sbathige:MxT7RMawJk6rNLCp@" /etc/yum.repos.d/edb.repo


# Install EPEL repository
dnf -y install epel-release

# Enable the PowerTools repository since EPEL packages may depend on packages from it
dnf config-manager --set-enabled PowerTools

# Install selected packages
dnf -y install edb-as12-server 

# Initialize Database cluster
PGSETUP_INITDB_OPTIONS="-E UTF-8" /usr/edb/as12/bin/edb-as-12-setup initdb

# Start Database server
systemctl start edb-as-12

# Connect to the database server
# sudo su - enterprisedb
# psql postgres

