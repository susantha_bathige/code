
##---------------------------------------------------------------
## My LAB
##---------------------------------------------------------------
## on both servers
CREATE ROLE replication WITH REPLICATION PASSWORD 'password' LOGIN
 

## On master
ALTER SYSTEM SET listen_addresses TO 'IP_standby_server';

# edit pg_hba.conf and add replication user with standby server ip. 
host    replication    replication  192.168.0.31/32 md5 


firewall-cmd --add-service=postgresql --permanent

##===================
## On stand by server
##===================
#Making a base backup

systemctl stop edb-as-12.service
sudo su - enterprisedb
cp -R /pg_data/data /pg_data/data/data_orig
rm -rf /pg_data/data/*

# use the pg_basebackup 
pg_basebackup -h 192.168.0.30 -D /pg_data/data -U replication -P -v -R -X stream -C -S pgstandby1

# start the server 
systemctl start edb-as-12.service
