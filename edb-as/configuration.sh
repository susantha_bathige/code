

##---------------------------------------------------
## My Lab
##---------------------------------------------------
# initdb
# creates a new cluster
/usr/edb/as12/bin/

# assign permission for the folders 
sudo chown enterprisedb:enterprisedb /pg_data
sudo chown enterprisedb:enterprisedb /pg_wal

# switch to the enterprisedb user
sudo su - enterprisedb

# setup file
/usr/edb/as12/bin/edb-as-12-setup

# specify the custom options
su - root
PGSETUP_INITDB_OPTIONS="-D /pg_data/data -X /pg_wal/wal" /usr/edb/as12/bin/edb-as-12-setup initdb

# Remove the following dir if the initdb failed. 
# Because evverytime when initdb runs, there will be a log file created, initdb.log.
sudo rm -rf /var/lib/edb/as12/data/

# Setup the systemctl correctly by referring the data directory
# Copy unit file
cp /usr/lib/systemd/system/edb-as-12.service /etc/systemd/system/

# After copying the unit file to the new location, create the service file
# change the below file with correct Data dir
/etc/systemd/system/edb-as-12.service

# also include the original unit file
include=/lib/systemd/system/edb-as-12.service

# reload the systemd
sudo systemctl daemon-reload

# enable and start the server
sudo systemctl enable edb-as-12.service
systemctl restart edb-as-12.service

# but still you can't connect to the server because of the error below;
# psql: error: could not connect to server: FATAL:  Peer authentication failed for user "postgres"
# change the pg_hba.conf file. 
Unix local connnections to trust

# Connecting to the server
sudo su - enterprisedb
psql postgres










