Install PEM Agent
Register PEM Agent

EDB Postgres Enterprise Manager (PEM)


Pem worker log
How to see this dir, sudo ls /var/log/pem/



To EDB

Do we need to install PEM agent on each DB server that we wish to monitor?
For the PEM server, do we need to install Postgres server as well?
What is the backing database?


permission to start 
httpd


pem server
edb-pem-server

pem agent
systemctl status pemagent



### installation steps
#01. Install Postgres 12 (Pre-requiste)
#02. Install PEM

To edb
======
How to create additional users on PEM
Registering PEM agent
-> where to put the .pgpass file
In Web UI, the same agent appears as three rows where one being shown as UP and other two as UNKNOWN. How can I get rid of the Unknown ones?


On ASC-PRD-PMON01
=================

Install pre-requisities
-----------------------
1. Install Postgres 12 community version. Since this is db server and it is used by the monitoring to store collected data, it needs two mountpoints for data and wal as it was configured in STG. (ATI-STG-PSQL01)
https://www.enterprisedb.com/edb-docs/d/edb-postgres-enterprise-manager/installation-getting-started/linux-installation-guide/7.16/prerequisites_for_installing_pem_server.html

configure Postgres to use the data and wal mount points. This has been already integrated to salt in STG server build by Tim D.

Install postgres extensions as stated in the above link. 
2. yum install postgresql12-contrib
3. yum install sslutils_12

Install Appache Web Server (pem-httpd)
--------------------------------------
https://www.enterprisedb.com/edb-docs/d/edb-postgres-enterprise-manager/installation-getting-started/linux-installation-guide/7.16/installing_pem_server_and_apache_web_server_preferences.html

Install PEM server
------------------
https://www.enterprisedb.com/edb-docs/d/edb-postgres-enterprise-manager/installation-getting-started/linux-installation-guide/7.16/installing_pem_server_on_linux.html


Install PEM Agent on following servers:
=======================================
ATI-STG-PSQL01
ATI-STG-PSQL02
https://www.enterprisedb.com/edb-docs/d/edb-postgres-enterprise-manager/installation-getting-started/linux-installation-guide/7.16/installing_pem_agent_on_linux.html


---======================Installation steps on PMON01

1. Install PostgreSQL 12 and initialized the cluster

2. create a user in postgres to use with PEM server

create user asc_pem_monitor with login superuser password 'n83;R_yNXZTw^SY' createrole createdb;
create user pem_admin with login superuser password 'E#d}V7)YC)' createrole createdb;
postgres e@?qkM&C3C

create user bv_pem_monitor with login password 

/*
Otherwise you will get the below error
-->  [Error] -----------------------------------------------------
-->  [Error] CRITICAL ERROR
-->  [Error] -----------------------------------------------------
-->  [Error] -->  [Error] Script exiting because of the following error:
-->  [Error] The specified user does not have permissions to create another role.
*/

3. Change the pg_hba.conf to md5 auth type

4. configure pem server
sudo /usr/edb/pem/bin/configure-pem-server.sh
# Enter the above command and follow the prompt
# sudoedit /usr/edb/pem/share/.install-config

## uninstalling pem server
sudo /usr/edb/pem/bin/configure-pem-server.sh --uninstall-pem-server

Make sure you can access Web UI if so you can proceed to the next step.

5. Configure pem agent on DB server

5.1 before that change the listen_address in postgresql.conf in monitoring server backend server to '*'
This requires a restart

5.2 do the following changes
mkdir /home/enterprisedb/.pem
mkdir /home/enterprisedb/.pem/logs
chmod 700 /home/enterprisedb/.pem
chmod 700 /home/enterprisedb/.pem/logs

<# what James did
mkdir /home/enterprisedb/.pem/logs -p
chown -R enterprisedb:enterprisedb /home/enterprisedb
#>

# also did this
touch /usr/edb/pem/agent/etc/agent.cfg
chown enterprisedb:enterprisedb /usr/edb/pem/agent/etc/agent.cfg
chmod 644 /usr/edb/pem/agent/etc/agent.cfg
 
5.3
## Eexcute this to register PEM Agent with PEM Server (PMON)
## Password is the password of the monitoring user in postgres in PMON server.
sudo -u enterprisedb PGPASSWORD='E#d}V7)YC)' /usr/edb/pem/agent/bin/pemworker --register-agent --enable-heartbeat-connection --cert-path /home/enterprisedb/.pem --force-registration yes
sudo -u enterprisedb PGPASSWORD='E#d}V7)YC)' /usr/edb/pem/agent/bin/pemworker --register-agent --enable-heartbeat-connection --cert-path /home/enterprisedb/.pem

# Registration was successfull. 

5.4 Start the agent

sudo systemctl start pemagent
sudo systemctl enable pemagent
sudo systemctl status pemagent

5.5 register the server
sudo -u enterprisedb PGPASSWORD='E#d}V7)YC)' /usr/edb/pem/agent/bin/pemworker --register-server
--ATI-STG-PSQL04
bv_stg_pem_monitor 'xU4S6E<hg:(n67'

# unregister server
sudo -u enterprisedb PGPASSWORD='E#d}V7)YC)' /usr/edb/pem/agent/bin/pemworker --unregister-server

## troubleshooting
sudo -u enterprisedb ls /home/enterprisedb/.pem/logs
less /var/log/pem/worker.log

<#
max_connections = 50
shared_buffers = 4GB
effective_cache_size = 12GB
maintenance_work_mem = 1GB
checkpoint_completion_target = 0.9
wal_buffers = 16MB
default_statistics_target = 100
random_page_cost = 1.1
effective_io_concurrency = 300
work_mem = 20971kB
min_wal_size = 2GB
max_wal_size = 8GB
max_worker_processes = 8
max_parallel_workers_per_gather = 4
max_parallel_workers = 8
max_parallel_maintenance_workers = 4
password_encryption = scram-sha-256
wal_keep_segments = 500
shared_preload_libraries = 'pg_stat_statements'
log_directory = '/var/log/postgres/'
log_truncate_on_rotation = on
log_rotation_age = 7d
#>

# cleared out everything and run salt highstate
sudo systemctl stop postgresql-12
sudo systemctl stop httpd
sudo systemctl stop pemagent
sudo yum remove -y edb-pem-agent edb-pem-server edb-pem
sudo rm -rf /var/lib/pemhome/
sudo rm -rf /srv/pgsql/12/data
sudo rm -rf /usr/edb

## register pem-agent
# pwd of the asc_pem_monitor
LD_LIBRARY_PATH=/usr/share/locale/cs/LC_MESSAGES/:${LD_LIBRARY_PATH} sudo -u postgres /usr/edb/pem/agent/bin/pemworker
export PGPASSWORD='password'
sudo -u postgres /usr/edb/pem/agent/bin/pemworker \ 
    --register-agent \
    --pem-server asc-prd-pmon02 \
    --pem-user asc_pem_monitor \
    --pem-port 5432 \
    --group ASC \
    --enable-heartbeat-connection


### Register server on ATI-STG-PSQL04
# password of the asc_pem_monitor of the PEM server
export PEM_SERVER_PASSWORD='n83;R_yNXZTw^SY'
# monitored db server password. prd pwd
export PEM_MONITORED_SERVER_PASSWORD='YWRkOWY5ZTFkZjNiZDA0YzhhNjYzZTc5'
# see whether you have permission to register
sudo /usr/edb/pem/agent/bin/pemworker --help

# register the server
sudo -u enterprisedb /usr/edb/pem/agent/bin/pemworker \
    --register-server \
    --pem-user asc_pem_monitor \
    --display-name ATI-STG-PSQL16 \
    --server-addr ATI-STG-PSQL16 \
    --server-port 5444 \
    --server-database postgres \
    --server-user pem_monitor_ati_stg \
    --efm-cluster-name efm \
    --efm-install-path /usr/edb/efm-4.4/bin \
    --remote-monitoring no \
    --group ATI

## troubleshooting
# find out libpq library path
rpm -ql libpq5-13.3-11PGDG.rhel7.x86_64 | grep libpq
# find out what libraries are used by a given binary
# Ldd is a powerful command-line tool that allows users to view an executable file’s 
#   shared object dependencies
ldd /usr/edb/pem/agent/bin/pemworker

## how Tim D did it. 
[7:34 AM] Tim Dreyer
    
sudo touch /usr/edb/pem/agent/etc/agent.cfg
sudo chown postgres:postgres !$
export PEM_SERVER_PASSWORD=''

LD_LIBRARY_PATH="/usr/lib64" /usr/edb/pem/agent/bin/pemworker --register-agent --pem-server asc-prd-pmon02.ad.ascendlearning.com --pem-user asc_pem_monitor --pem-port 5432 --enable-heartbeat-connection --cert-path /srv/pgsql/9.6/.pem

## register ASC-STG-PSQL01
# create user asc_stg_pem_monitor with login password '~CwkHCbzV8Qxzv3V'
export PEM_SERVER_PASSWORD='n83;R_yNXZTw^SY'
export PEM_MONITORED_SERVER_PASSWORD='~CwkHCbzV8Qxzv3V'
sudo -u postgres LD_LIBRARY_PATH="/usr/lib64" /usr/edb/pem/agent/bin/pemworker \
    --register-server \
    --pem-user asc_pem_monitor \
    --display-name ASC-STG-PSQL01 \
    --server-addr ASC-STG-PSQL01 \
    --server-port 5432 \
    --server-database postgres \
    --server-user asc_stg_pem_monitor \
    --remote-monitoring no \
    --group ASC
