#connected to the primary;

# connecting to mongodb from shell
mongo "mongodb://dozer.ideafit.com:27017/admin"

1. rs.status() - replica set status
2. show dbs 
3. rs.printSlaveReplicationInfo() - to see the replication lag with the master
4. rs.printReplicationInfo() - check the size of the oplog. 
#Note: The oplog should be long enough to hold all transactions for the longest downtime you expect on a secondary.
5. rs.isMaster() - Gives info about the whole set. 
6. db.currentOp() - Current operations. Can find out long running queries. 
7. rs.conf()
8. rs.conf()['members']



# following commands should run during a downtime. 
1. rs.reconfig() 
# can make Primary to step down which usually causes an election to choose a new Primary. 
# Recommendation is to make these changes during scheduled downtime.

