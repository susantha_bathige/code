
## how to configure client
https://tylersguides.com/guides/configuring-ldap-authentication-on-centos-8/

## Need to add below host entry on each client
192.168.0.160   dbt-ldap01.dbtorque.su

## how to search
ldapsearch -x -h dbt-ldap01.dbtorque.su \
-D "uid=testuser,ou=users,dc=dbtorque,dc=su" \
-b "ou=users,dc=dbtorque,dc=su"

## service
systemctl enable nslcd
systemctl restart nslcd

### ==============================================================================
### cat /etc/nslcd.conf
### ==============================================================================
# The user and group name the cache daemon will run as.
uid nslcd
gid ldap

# Make sure to use the fully qualified domain name or the TLS handshake
# will fail and you won't be able to connect.

# The URI if your directory server(s). Multiple servers can be specified.
uri ldaps://dbt-ldap01.dbtorque.su/
# uri ldaps://ldap2.dbtorque.com

# This should be your suffix.
base dc=dbtorque,dc=su

# The LDAP user the cache daemon and modules will use for looking
# up entries
binddn cn=osproxy,ou=system,dc=dbtorque,dc=su

# The password for the aforementioned account
bindpw <enter the pwd in clear text>

# Where to look for groups and users, respectively.
base   group  ou=groups,dc=dbtorque,dc=su
base   passwd ou=users,dc=dbtorque,dc=su

# How long, in seconds, to wait for the server to respond when logging in
# as the aforementioned user before giving up
bind_timelimit 30

# How long to wait for the server when searching for entries.
timelimit 30

# TLS  configuration. tls_reqcert demand will prevent the daemon and modules
# from using the server if the server certificate does not have a signing chain
# that ends with a root certificate listed in the file set by tls_cacertfile
# comment out the rest of the file if you do not plan on using TLS. If you
# opt to not use TLS, passwords will be sent through the network in plain text
ssl on
tls_reqcert demand
# If your directory server uses a certificate from a well known CA,
# comment out the next line and uncomment the one below it.
tls_cacertfile /pki/ldap_server_certs.pem
#tls_cacertfile /etc/ssl/certs/ca-bundle.crt
tls_ciphers HIGH:TLSv1.2:!aNULL:!eNULL
### ==========================EOF==================================================

## common errors
<passwd=0> failed to bind to LDAP server ldaps://dbt-ldap01.dbtorque.su/: Invalid credentials
[8b4567] <passwd=0> no available LDAP server found: Invalid credentials: Bad file descriptor
--> resolution-> binddn had wrong value for 'ou'. I changed it to system. 
