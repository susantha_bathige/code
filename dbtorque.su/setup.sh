## Add a user to a group
# here, susanthab is the doamin user
# before that you must edit the file /etc/sssd/sssd.conf as stated below and restart sssd
usermod -aG wheel susanthab

## Identify network card properties
cat /etc/sysconfig/network-scripts/ifcfg-ens192

##--------------------------------------------------------------
# Add Linux nnode to the AD 
##--------------------------------------------------------------
dnf -y install realmd sssd oddjob oddjob-mkhomedir adcli samba-common-tools krb5-workstation

# To AD 
# AD: 192.168.0.160
nmcli connection modify ens192 ipv4.dns 192.168.0.160

# reset the network
nmcli connection down ens192; nmcli connection up ens192

# discover the domain
realm discover dbtorque.su

# add to the domain
realm join dbtorque.su

# verify
id susanthab@dbtorque.su

# if you want to omit domain name for the AD user
[root@dlp ~]# vi /etc/sssd/sssd.conf
# line 16: change
use_fully_qualified_names = False
[root@dlp ~]# systemctl restart sssd

# Reference
https://www.server-world.info/en/note?os=CentOS_8&p=realmd


# Ubuntu server extend the disk
sudo lvextend -L+100G /dev/ubuntu-vg/ubuntu-lv  resize2fs /dev/ubuntu-vg/ubuntu-lv

# start postgres
/usr/lib/postgresql/13/bin/pg_ctl -D /pgsql/13/data/ -l logfile start
systemctl status system-postgresql.slice

