## conf files
/opt/openldap-current/etc/openldap/slapd.ldif
systemctl daemon-reload

## how to search
ldapsearch -x -W -H ldapi:/// \
-D "uid=testuser,ou=users,dc=dbtorque,dc=su" \
-b "ou=users,dc=dbtorque,dc=su"

# generate LDAP pwd
# add the HASH to the rootdn.ldif file. 
slappasswd

## update the db
ldapadd -Y EXTERNAL -H ldapi:/// -f rootdn.ldif

## add a new user
# https://tylersguides.com/guides/openldap-how-to-add-a-user/
# create a ldif file with user info
# load the file
# set a pwd

## ldap service
systemctl status slapd-current

## ldap config testing
slaptest -u

## view certificate
openssl s_client -connect dbt-ldap01.dbtorque.su:636 -showcerts < /dev/null

## add firewall rules
firewall-cmd --add-service={ldap,ldaps} --permanent
firewall-cmd --reload

## how to delete a user
# single user
ldapdelete "uid=testuser,ou=users,dc=dbtorque,dc=su"
# bulk delete
ldapdelete -f deletefile

## change pwd
ldappasswd "uid=testuser2,ou=users,dc=dbtorque,dc=su"

## References
https://tylersguides.com/guides/configuring-openldap-for-linux-authentication/
https://kifarunix.com/install-and-setup-openldap-on-centos-8/
https://tylersguides.com/guides/managing-openldap/

###=========================================================================
### cat rootdn.ldif
###=========================================================================
dn: olcDatabase=mdb,cn=config
objectClass: olcDatabaseConfig
objectClass: olcMdbConfig
olcDatabase: mdb
olcSuffix: dc=dbtorque,dc=su
olcRootDN: cn=admin,dc=dbtorque,dc=su
olcRootPW: {SSHA}Lfiilxeyw8kww1x+FO7ePTNdIBqsFNfo
olcDbDirectory: /ldapdata
olcDbIndex: objectClass,uid,uidNumber,gidNumber eq
olcDbMaxSize: 10485760
olcAccess: to attrs=userPassword
  by self write
  by anonymous auth
  by dn.subtree="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" manage
  by * none
olcAccess: to attrs=shadowLastChange by self write
  by dn.subtree="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" manage
  by dn.subtree="ou=system,dc=dbtorque,dc=su" read
  by * none
olcAccess: to dn.subtree="ou=system,dc=dbtorque,dc=su" by dn.subtree="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" manage
  by * none
olcAccess: to dn.subtree="dc=dbtorque,dc=su" by dn.subtree="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" manage
  by users read
  by * none
###===========================EOF===========================================

###=========================================================================
### cat data.ldif
###=========================================================================
dn: dc=dbtorque,dc=su
objectClass: dcObject
objectClass: organization
objectClass: top
o: susantha bathiges DC
dc: dbtorque

dn: ou=groups,dc=dbtorque,dc=su
objectClass: organizationalUnit
objectClass: top
ou: groups

dn: ou=users,dc=dbtorque,dc=su
objectClass: organizationalUnit
objectClass: top
ou: users

dn: ou=system,dc=dbtorque,dc=su
objectClass: organizationalUnit
objectClass: top
ou: system

dn: cn=osproxy,ou=system,dc=dbtorque,dc=su
objectClass: organizationalRole
objectClass: simpleSecurityObject
cn: osproxy
userPassword: {SSHA}gnu86UrQM1S58kjCqSEy0MEAQPk23VKz
description: OS proxy for resolving UIDs/GIDs

dn: cn=testgroup,ou=groups,dc=dbtorque,dc=su
objectClass: posixGroup
cn: testgroup
gidNumber: 5000
memberUid: testuser

dn: uid=testuser,ou=users,dc=dbtorque,dc=su
objectClass: posixAccount
objectClass: shadowAccount
objectClass: inetOrgPerson
cn: First Name
sn: Last Name
uid: testuser
uidNumber: 5000
gidNumber: 5000
homeDirectory: /home/testuser
loginShell: /bin/sh
gecos: Full Name
###=======================EOF===============================================

## load ldif file
ldapadd -Y EXTERNAL -H ldapi:/// -f data.ldif

## references
https://tylersguides.com/guides/configuring-openldap-for-linux-authentication/

