
# On my mac kube config file is in a wired location. 
/System/Volumes/Data/Users/susantha.bathige/.kube/config
# I reinstalled the kubectl and then the .kube is created on my home dir. 

# To view the config
kubectl config view

# To view the current context
kubectl config current-context

# To list all contexts
kubectl config get-contexts

# Change context
kubectl config use-context CONTEXT_NAME

# Change context default ns
#   So that you do not have to specify the context all the time. 
kubectl config set-context asc-stg-ham-tenant-01 --namespace dba-postgres-operator-ham-dev

kubectl get apiservices

## References
# Internal resource
https://wiki.ascendlearning.com/display/SRE/Kubernetes+CLI+-+kubectl#KubernetesCLIkubectl-MacOSwithHomebrew


## Dealing with secrets




