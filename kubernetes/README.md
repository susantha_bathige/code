## minikube setup
# follow the docs , install docker first 
# also the following
yum install conntrack

## start on root.
minikube start --driver=none

## install kubectl 
https://kubernetes.io/docs/tasks/tools/install-kubectl/

## create sample table
`create': create table t_test1 as select * from generate_series(1,100000) as Id,
random()*Id as data;

## enable addons
minikube addons enable ingress


