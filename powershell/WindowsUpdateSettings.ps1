try {
  $objAutoUpdate = New-Object -ComObject "Microsoft.Update.AutoUpdate" 
  $objAutoUpdate.Settings
   
  # The rest of this is just static info on the meaning of various Settings.
  "NotificationLevel:"
  "1 - Never check for updates"
  "2 - Check for updates but let me choose whether to download and install them"
  "3 - Download updates but let me choose whether to install them"
  "4 - Install updates automatically"
  ""
  "ScheduledInstallationDay"
  "0 - Every day"
  "1-7 - Sunday through Saturday"
  "Note:  On Windows 8/2012 and later, ScheduledInstallationDay and"
  "       ScheduledInstallationTime are only reliable if the values" 
  "       are set through Group Policy."
  ""
  "Script execution succeeded"
  $ExitCode = 0
}
catch {
  ""
  $error[0]
  ""
  "Script execution failed"
    $ExitCode = 1001 # Cause script to report failure in GFI dashboard
}
 