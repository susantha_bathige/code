

# Check functions availability
dir Function:\Get-Version

# Remove a function
Get-ChildItem -Path Function:\Get-*Version | Remove-Item

# dot source PS functions
# Module files should be names as .psm1
# You can place these modules in PSModule path and that will be automatically load. 
. .\module.psm1

# How to check PSModule paths
```bash
$env:PSModulePath -Split ';'
```

# Advanced functions
[cmdLetBinding()] <- this gives common parameters to the functions
Write-Verbose -Message "This is a verbose msg"
In most cases Write-Host is a bad practice.  
Then execute your function with -Verbose switch. 

# WhatIf
Run the cmd without actually running it. 

# Parameter validation
[string]$ComputerName -> This means single string. Not a array of strings. 
[string[]]$ComputerName -> Array of strings
[Parameter(Mandatory)] -> If you specify it's true, if you do not specify, it's false. 
Default values do not work with mandatory parameters. Below is what you need to use in that case. 
[ValidateNotNullorEmpty()]
Validate IP address
[ipaddress]$IPAddress
See all the type accelerators
[psobject].Assembly.GetType('System.Management.Automation.TypeAccelerators')::Get | Sort-Object -Property Value
You can add pipeline input capability to PS function. See below
[Parameter(Mandatory, ValueFromPipeline)]
[Parameter(Mandatory, ValueFromPipelineByPropertyName)] -> $object | Test-Function


# ShortCuts ISE
CTL+J -> See the templates

# Add color parameters to the function
param(
    [ValidateNotNullOrEmpty()]
    [System.ConsoleColor[]]$Color = [System.Enum]::GetValues([System.ConsoleColor])
)
There are so many built-in Enums. Get-Type -BaseType System.Enum
[System.Enum]::GetValues([System.DayOfWeek])

# Error handling
The magic is to handle unhandled exception is to use -ErrorAction Stop
try{
    Test-WSMan -ComputerName $Computer -ErrorAction Stop
}
catch{
    Write-Warning -Message "Unable to connect to Computer: $Computer" 
}
