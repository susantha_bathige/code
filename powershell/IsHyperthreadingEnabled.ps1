$cpu = Get-WmiObject Win32_Processor | Select -First 1
$isHyperThreadingEnabled = $cpu.NumberOfLogicalProcessors -gt $cpu.NumberOfCores

Write-Host "Is HyperThreading Enabled? $isHyperThreadingEnabled"