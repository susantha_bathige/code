
Get-EventLog System -After (Get-Date).addDays(-1) | Where-Object {$_.EntryType -match "Error"} | 
    Select-Object -Property TimeGenerated,MachineName,Message |Format-Table
Get-EventLog Security
Get-EventLog Application