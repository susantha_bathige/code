﻿
File CreateDataDir {
    DestinationPath = 'D:\temp-ps-dsl'
    Ensure = 'Present'
    Type = 'Directory'A4LZMwOX3wLNZ4ubzq8RUn5JJAZi06I3
}

Get-DscResource -Module PSDesiredStateConfiguration
Get-DscResource -Name File -Syntax # Get the exact syntax 

# Discover resources in the gallery
Find-DscResource -Name SqlServerDsc

Get-Command -CommandType Configuration


Find-Module -Name SqlServerDsc | Install-Module
Find-module -Name SecurityPolicyDsc | Install-Module
Find-Module -Name xSQLServer

Get-DscResource -Module SqlServerDsc
Get-DscResource -Module SecurityPolicyDsc -Syntax
