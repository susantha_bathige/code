if (true) {
    Write-Host "I'm PS Core from Mac"
}
Write-Host "I'm PS Core from Mac"

# rename output column
get-process | select-object -Property Name, @{name='procid';expression={$_.id}}

Get-Process w* | clip

Function get-hashtable {
    @{
        First = 'Susantha'
        Last = 'Bathige'
        City = 'Littleton'
    }
}

get-hashtable

function Get-PSCustomObject {
    [PSCustomObject]@{
        First = 'Susantha'
        Last = 'Bathige'
        City = 'Littleton'
    }
}

Get-PSCustomObject

# class definition
Class Person {
    [String] $First
    [String] $Last
    [String] $City
}

function Get-Instance {
    [Person]@{
        First = 'Susantha'
        Last = 'Bathige'
        City = 'Littleton' 
    }
}

Get-Instance

# create the instance by using the constructor
$person = [Person]::new()
$person.First = 'Susantha'
$person.Last = 'Bathige'
$person.City = 'Litteton'

# classes with methods
Class Person3 {
    [String] $First = 'Leela'
    [String] $Last = 'Wattuhewa'
    [string] $City = 'Littleton'

    # Methods
    [String] GetFullName()
    {
        return ("{1}, {0}" -f $this.First, $this.Last)   
    }

    [void] Wave() {
        Write-Verbose -Verbose ("{0} waves hellow" -f $this.First)
    }
}

$person = [Person3]::new()
$person.GetFullName()
$person.Wave()


# Constructors
Class Person5 {
    [String] $First
    [String] $Last 
    [string] $City

    Person5($First, $Last, $City){
        $this.First = $First
        $this.Last = $Last
        $this.City = $City
    }
    # default constructor
    Person5(){}
}

[Person5]::new('Susantha','Bathige','Littleton')
[Person5]::new()
