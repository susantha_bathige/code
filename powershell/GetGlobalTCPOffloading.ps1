# This works only for Windows 2012 +
#gets the global TCP/IP offload settings
#http://technet.microsoft.com/en-us/library/hh826133.aspx
Get-NetOffloadGlobalSetting

# for Windows 2008
# https://support.microsoft.com/en-us/kb/2643970?wa=wsignin1.0
netsh interface tcp show global

netsh interface tcp show chimneyapplications
netsh interface tcp show chimneyports

