
$sqlsvr = 'SUSANTHABWIN7\SQL2012'
$database = 'tempdb'
$table = 'Test'

$User = 'UserName'
$Password =  'Password100'

#Create SQL Connection
Write-Verbose "Creating SQL Connection"
$conn = New-Object System.Data.SqlClient.SqlConnection("Data Source=$SQLSvr;Initial Catalog=$Database; Integrated Security=SSPI")
$conn.Open()
$cmd = $conn.CreateCommand()

Import-Csv D:\temp\ESM_Leads_Received_Daily_2013_1209.csv | % {

  Write-Host '$($_.Lead_ID)'
  
  #Create query string
  #Must matching the table layout (SoftwareName, Version)
  Write-Host -Fore Green "Updateing Table"
  $cmd.CommandText = "INSERT INTO $table (Lead_ID, Date_Added, Script_Name, POI, First_Name, Last_Name, Email, Phone_Number, ESM_MarketingVendor_Source) VALUES ('$($_.Lead_ID)','$($_.Date_Added)', '$($_.Script_name)','$($_.POI)','$($_.First_Name)', '$($_.Last_Name)','$($_.Email)','$($_.Phone_Number)','$($_.ESM_MarketingVendor_Source)')" 
  #$cmd.CommandText = "INSERT INTO $table (Lead_ID,Date_Added,Script_Name,POI,First_Name) VALUES ('$($_.Lead_ID)', '$($_.Date_Added)', '$($_.Script_name)','$($_.POI)')" 

  #Execute Query
  $cmd.ExecuteNonQuery() | Out-Null 
  }