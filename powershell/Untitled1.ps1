﻿#Enter-PSSession -ComputerName asc-prd-jmp01
#$cred = Get-Credential

##Save the below powershell script in folder "D:\ExecScriptOnMultiServer\ExecuteQueryOnMultiServers.ps1"
#clear;

$QueryPath= "E:\Ashish\AuditConfig\0.CreateDB_Admin.sql"
$OutputFile = "E:\Ashish\AuditConfig\QueryOutput\QueryOutput.txt"


$ExecuteQuery= Get-Content -path $QueryPath | out-string

#"Results -- > `r`n`r`n" > $OutputFile

FOREACH($server in GC "E:\Ashish\AuditConfig\AuditServers.txt")
 {
	$server 

	"---------------------------------------------------------------------------------------------------------" >> $OutputFile
	$server >> $OutputFile
	"---------------------------------------------------------------------------------------------------------" >> $OutputFile
	invoke-sqlcmd -ServerInstance $server -Credentials ascend\sbathige-admin -query $ExecuteQuery -querytimeout 65534 | ft -autosize | out-string -width 4096 >> $OutputFile
 }

 #Exit-PSSession