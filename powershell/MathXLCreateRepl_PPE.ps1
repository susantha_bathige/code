
#---------------------------------------------------------------------
# for school (PPE) 
#---------------------------------------------------------------------

$ScriptFolder = "W:\deltanetschool\"
$InputTxtFile = $ScriptFolder + 'ListOfTable.txt'
$outFile = $ScriptFolder + 'output.txt'
foreach ($ListOfTables in Get-Content $InputTxtFile)
{
    $FileName=$ScriptFolder + $ListOfTables +'.sql'
    $FileName 
    sqlcmd -E -S "SQL2012XL105B03" -v SubscriberServer="'SQL2012XL106B03\I02'" -i $FileName >> $outFile
}

#---------------------------------------------------------------------
# for global (PPE) 
#---------------------------------------------------------------------

$ScriptFolder = "W:\deltanetglobal\"
$InputTxtFile = $ScriptFolder + 'ListOfTable.txt'
$outFile = $ScriptFolder + 'output.txt'
foreach ($ListOfTables in Get-Content $InputTxtFile)
{
    $FileName=$ScriptFolder + $ListOfTables +'.sql'
    $FileName 
    sqlcmd -E -S "SQL2012XL105B03" -v SubscriberServer="'SQL2012XL106B03\I02'" -i $FileName >> $outFile
}

#---------------------------------------------------------------------
# for deltanet (HED)
#---------------------------------------------------------------------

$ScriptFolder = "W:\deltanet\scripts\"
$InputTxtFile = $ScriptFolder + 'ListOfTable.txt'
$outFile = $ScriptFolder + 'output.txt'
foreach ($ListOfTables in Get-Content $InputTxtFile)
{
    $FileName=$ScriptFolder + $ListOfTables +'.sql'
    $FileName 
    sqlcmd -E -S "SQL2012XL100B03" -v SubscriberServer="'SQL2012XL101B03\I02'" -v SubscriberServer2="'SQL2012XL102B03\I03'" -i $FileName >> $outFile
}