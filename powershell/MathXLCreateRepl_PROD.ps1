
# PROD

#---------------------------------------------------------------------
# for deltanet (HED)
#---------------------------------------------------------------------

$ScriptFolder = "W:\deltanetreplication\scripts\"
$InputTxtFile = $ScriptFolder + 'ListOfTable.txt'
$outFile = $ScriptFolder + 'output.txt'
foreach ($ListOfTables in Get-Content $InputTxtFile)
{
    $FileName=$ScriptFolder + $ListOfTables +'.sql'
    $FileName 
    sqlcmd -E -S "SQL2012XL600B03" -v SubscriberServer="'SQL2012XL601B03\I02'" -v SubscriberServer2="'SQL2012XL602B03\I03'" -i $FileName >> $outFile
}


#---------------------------------------------------------------------
# for school  
#---------------------------------------------------------------------

$ScriptFolder = "W:\school_global_replication\school\scripts\"
$InputTxtFile = $ScriptFolder + 'ListOfTable.txt'
$outFile = $ScriptFolder + 'output.txt'
foreach ($ListOfTables in Get-Content $InputTxtFile)
{
    $FileName=$ScriptFolder + $ListOfTables +'.sql'
    $FileName 
    sqlcmd -E -S "SQL2012XL605B03" -v SubscriberServer="'SQL2012XL606B03\I02'" -i $FileName >> $outFile
}

#---------------------------------------------------------------------
# for global 
#---------------------------------------------------------------------

$ScriptFolder = "W:\repl_global\"
$InputTxtFile = $ScriptFolder + 'ListOfTable.txt'
$outFile = $ScriptFolder + 'output.txt'
foreach ($ListOfTables in Get-Content $InputTxtFile)
{
    $FileName=$ScriptFolder + $ListOfTables +'.sql'
    $FileName 
    sqlcmd -E -S "SQL2012XL605B03" -v SubscriberServer="'SQL2012XL606B03\I02'" -i $FileName >> $outFile
}

