-- 
```bash
ps -ef | grep vacuum
```

```text
Gather below info before tuning the vacuum parameters.
Number of rows in each table
Number of dead tuples in each table
The time of the last vacuum for each table
The time of last analyze for each table
The rate of data insert/update/delete in each table
The time taken by autovacuum for each table
Warnings about tables not being vacuumed
Current performance of most critical queries and the tables they access
Performance of the same queries after a manual vacuum/analyze
```

```sql
-- check the progress of vacuum
select * from pg_stat_progress_vacuum where pid = 343;
select * from pg_stat_progress_analyze;

-- auto vacuum workers
-- typical you can go as higher as 5

-- vacuum options
VACUUM (DISABLE_PAGE_SKIPPING, SKIP_LOCKED, VERBOSE) my_table; 
VACUUM (DISABLE_PAGE_SKIPPING ON, SKIP_LOCKED ON, VERBOSE ON, ANALYZE OFF) my_table;
--INDEX_CLEANUP (default true)
vacuum (freeze, analyze, verbose) responses;

/*
set vacuum_cost_delay = 10
VACUUM (DISABLE_PAGE_SKIPPING ON, SKIP_LOCKED ON, VERBOSE ON, ANALYZE OFF) my_table;
select * from pg_stat_progress_vacuum;
*/

-- reindex
reindex index concurrently <index name>;

-- What are the best times of day to do things? When are system resources more available?
-- Which days are quite, which are not?
-- Which tables are critical to the application, and which are not?

autovacuum
track_counts

-- Vacuum related parameters
show vacuum_cleanup_index_scale_factor;
show vacuum_cost_delay;
show vacuum_cost_limit;
vacuum_cost_page_dirty;
vacuum_cost_page_hit; 
vacuum_cost_page_miss; 
vacuum_defer_cleanup_age; 
vacuum_failsafe_age; 
vacuum_freeze_min_age; 
vacuum_freeze_table_age;
vacuum_multixact_freeze_min_age; 
vacuum_multixact_freeze_table_age;

-- autovacuum related parameters
-- these parameters apply to all the tables
show autovacuum;
show autovacuum_analyze_scale_factor; 
show autovacuum_analyze_threshold; 
show autovacuum_freeze_max_age; 
show autovacuum_max_workers; 
show autovacuum_multixact_freeze_max_age; 
show autovacuum_naptime; 
show autovacuum_vacuum_cost_delay; 
show autovacuum_vacuum_cost_limit; 
show autovacuum_vacuum_insert_threshold; 
show autovacuum_vacuum_insert_scale_factor;
show autovacuum_vacuum_scale_factor; 
show autovacuum_vacuum_threshold; 
show autovacuum_work_mem; 
show log_autovacuum_min_duration;

-- individual tables can be controlled by storage parameters
ALTER TABLE mytable SET (storage_parameter = value);
-- storage parameters
show autovacuum_enabled; 
show autovacuum_analyze_scale_factor; 
show autovacuum_analyze_threshold; 
show autovacuum_freeze_min_age; 
show autovacuum_freeze_max_age; 
show autovacuum_freeze_table_age; 
show autovacuum_multixact_freeze_max_age; 
show autovacuum_multixact_freeze_min_age; 
show autovacuum_multixact_freeze_table_age; 
show autovacuum_vacuum_cost_delay; 
show autovacuum_vacuum_cost_limit; 
show autovacuum_vacuum_insert_threshold; 
show autovacuum_vacuum_insert_scale_factor;
show autovacuum_vacuum_scale_factor; 
show autovacuum_vacuum_threshold; vacuum_truncate; (no equivalent postgresql.conf parameter) 
show log_autovacuum_min_duration;

-- if you want big table to vacuum more quickly
ALTER TABLE big_table SET (autovacuum_vacuum_cost_delay = 0);

-- disable auto vacuum for toast table
ALTER TABLE pgbench_accounts SET ( toast.autovacuum_enabled = off);

SELECT n.nspname
	,c.relname
	,array_to_string(c.reloptions || ARRAY(SELECT 'toast.' || x FROM unnest(tc.reloptions) AS x), ', ') AS relopts
FROM pg_class c
LEFT JOIN pg_class tc ON c.reltoastrelid = tc.oid
JOIN pg_namespace n ON c.relnamespace = n.oid
WHERE c.relkind = 'r'
	AND nspname NOT IN (
		'pg_catalog'
		,'information_schema'
		);


-- age of oldest snapshots that are running
-- if sessions are stuck in "idle in transaction" state, then consider adjusting idle_in_transaction_session_timeout parameter. 
-- or old_snapshot_threshold 
SELECT now() - CASE 
		WHEN backend_xid IS NOT NULL
			THEN xact_start
		ELSE query_start
		END AS age
	,pid
	,backend_xid AS xid
	,backend_xmin AS xmin
	,STATE
FROM pg_stat_activity
WHERE backend_type = 'client backend'
ORDER BY 1 DESC;

-- size of internal tables
SELECT relname, pg_relation_size(oid) FROM pg_class WHERE relkind in ('i','r') AND relnamespace = 'pg_catalog'::regnamespace ORDER BY 2 DESC;

-- check vaccum status
CREATE
	OR REPLACE VIEW av_needed AS
SELECT N.nspname
	,C.relname
	,pg_stat_get_tuples_inserted(C.oid) AS n_tup_ins
	,pg_stat_get_tuples_updated(C.oid) AS n_tup_upd
	,pg_stat_get_tuples_deleted(C.oid) AS n_tup_del
	,CASE 
		WHEN pg_stat_get_tuples_updated(C.oid) > 0
			THEN pg_stat_get_tuples_hot_updated(C.oid)::REAL / pg_stat_get_tuples_updated(C.oid)
		END AS HOT_update_ratio
	,pg_stat_get_live_tuples(C.oid) AS n_live_tup
	,pg_stat_get_dead_tuples(C.oid) AS n_dead_tup
	,C.reltuples AS reltuples
	,round(COALESCE(threshold.custom, current_setting('autovacuum_vacuum_threshold'))::INTEGER + COALESCE(scale_factor.custom, current_setting('autovacuum_vacuum_scale_factor'))::NUMERIC * C.reltuples) AS av_threshold
	,date_trunc('minute', greatest(pg_stat_get_last_vacuum_time(C.oid), pg_stat_get_last_autovacuum_time(C.oid))) AS last_vacuum
	,date_trunc('minute', greatest(pg_stat_get_last_analyze_time(C.oid), pg_stat_get_last_analyze_time(C.oid))) AS last_analyze
	,pg_stat_get_dead_tuples(C.oid) > round(current_setting('autovacuum_vacuum_threshold')::INTEGER + current_setting('autovacuum_vacuum_scale_factor')::NUMERIC * C.reltuples) AS av_needed
	,CASE 
		WHEN reltuples > 0
			THEN round(100.0 * pg_stat_get_dead_tuples(C.oid) / reltuples)
		ELSE 0
		END AS pct_dead
FROM pg_class C
LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace) NATURAL
LEFT JOIN LATERAL(SELECT (regexp_match(unnest, '^[^=]+=(.+)$')) [1] FROM unnest(reloptions) WHERE unnest ~ '^autovacuum_vacuum_threshold=') AS threshold(custom) NATURAL
LEFT JOIN LATERAL(SELECT (regexp_match(unnest, '^[^=]+=(.+)$')) [1] FROM unnest(reloptions) WHERE unnest ~ '^autovacuum_vacuum_scale_factor=') AS scale_factor(custom)
WHERE C.relkind IN (
		'r'
		,'t'
		,'m'
		)
	AND N.nspname NOT IN (
		'pg_catalog'
		,'information_schema'
		)
	AND N.nspname NOT LIKE 'pg_toast%'
ORDER BY av_needed DESC
	,n_dead_tup DESC;

--
SELECT * FROM av_needed WHERE nspname = 'public' AND relname = 'pgbench_accounts';

-- whether indexes are changing over time. 
SELECT nspname,relname, round(100 * pg_relation_size(indexrelid) / pg_relation_size(indrelid)) / 100 AS index_ratio,      pg_size_pretty(pg_relation_size(indexrelid)) AS index_size, pg_size_pretty(pg_relation_size(indrelid)) AS table_size FROM pg_index I LEFT JOIN pg_class C ON (C.oid = I.indexrelid) LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace) WHERE nspname NOT IN ('pg_catalog', 'information_schema', 'pg_toast') AND C.relkind='i' AND
pg_relation_size(indrelid) > 0;

-- do not run pgstattuple on PROD.
--SELECT * FROM pgstattuple('pg_catalog.pg_proc');

-- vacuum formats
VACUUM (DISABLE_PAGE_SKIPPING, SKIP_LOCKED, VERBOSE) my_table; 
VACUUM (DISABLE_PAGE_SKIPPING ON, SKIP_LOCKED ON, VERBOSE ON, ANALYZE OFF) my_table;

-- index usage
SELECT schemaname, relname, indexrelname, idx_scan FROM pg_stat_user_indexes ORDER BY idx_scan;

-- to check wraparound transaction issue
-- ref - https://www.crunchydata.com/blog/managing-transaction-id-wraparound-in-postgresql
WITH max_age AS (
    SELECT 2000000000 as max_old_xid
        , setting AS autovacuum_freeze_max_age
        FROM pg_catalog.pg_settings
        WHERE name = 'autovacuum_freeze_max_age' )
, per_database_stats AS (
    SELECT datname
        , m.max_old_xid::int
        , m.autovacuum_freeze_max_age::int
        , age(d.datfrozenxid) AS oldest_current_xid
    FROM pg_catalog.pg_database d
    JOIN max_age m ON (true)
    WHERE d.datallowconn )
SELECT max(oldest_current_xid) AS oldest_current_xid
    , max(ROUND(100*(oldest_current_xid/max_old_xid::float))) AS percent_towards_wraparound
    , max(ROUND(100*(oldest_current_xid/autovacuum_freeze_max_age::float))) AS percent_towards_emergency_autovac
FROM per_database_stats

SELECT datname
    , age(datfrozenxid)
    , current_setting('autovacuum_freeze_max_age')
FROM pg_database
ORDER BY 2 DESC;

SELECT c.oid::regclass
    , age(c.relfrozenxid)
    , pg_size_pretty(pg_total_relation_size(c.oid))
FROM pg_class c
JOIN pg_namespace n on c.relnamespace = n.oid
WHERE relkind IN ('r', 't', 'm')
AND n.nspname NOT IN ('pg_toast')
ORDER BY 2 DESC LIMIT 100;

-- given by EDB support
SELECT pid, datname, usename, state, backend_xmin
FROM pg_stat_activity
WHERE backend_xmin IS NOT NULL
ORDER BY age(backend_xmin) DESC;

SELECT gid, prepared, owner, database, transaction AS xmin
FROM pg_prepared_xacts
ORDER BY age(transaction) DESC;

--- get vacuum settings
select name, setting from pg_settings where name ~ 'vacuum' order by name;

-- tables with high dead tuples
select now() as snaptime,relname,n_live_tup,n_tup_ins,n_tup_upd,n_tup_del,n_dead_tup,last_autovacuum,n_mod_since_analyze,last_autoanalyze,n_dead_tup*1.0/(n_live_tup+1) dead_to_live_tup_ratio
from pg_catalog.pg_stat_all_tables psat
order by n_dead_tup desc
limit 10
;

-- set autovacuum settings at table level
--set 
alter table responses set (autovacuum_vacuum_scale_factor = 0);
alter table responses set (autovacuum_vacuum_threshold = 5000000);
--current setting is 600
alter table responses set (autovacuum_vacuum_cost_limit = 400);

--verify
SELECT reloptions FROM pg_class WHERE relname='responses'

--rollback
alter table responses reset ('autovacuum_vacuum_scale_factor','autovacuum_vacuum_threshold');
```








