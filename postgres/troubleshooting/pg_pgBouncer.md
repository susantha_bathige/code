
## How to connect

## Admin console
```text
pgbouncer=# SHOW help;
NOTICE:  Console usage
DETAIL:  
    SHOW HELP|CONFIG|DATABASES|POOLS|CLIENTS|SERVERS|VERSION
    SHOW FDS|SOCKETS|ACTIVE_SOCKETS|LISTS|MEM
    SHOW DNS_HOSTS|DNS_ZONES
    SHOW STATS|STATS_TOTALS|STATS_AVERAGES
    SET key = arg
    RELOAD
    PAUSE [<db>]
    RESUME [<db>]
    DISABLE <db>
    ENABLE <db>
    KILL <db>
    SUSPEND
    SHUTDOWN
```

## SHOW pools
```text
Clients are the one connecting into the pgbouncer, servers connect out to the actual DB server. 
```

## Admin commands from cmd line
```bash
pgbouncer -R ?

pgrep -alf pgbouncer
```

## Restart the DB
```text
-- pause pgbouncer
PAUSE <database name> # logical db name defined in the config file
restart db service
RESUME <database name>
```

## Restart pgbouncer
```bash
# You even can point to a new config.
pgbouncer -R /etc/pgbouncer/pgbouncer_custom.ini
```

# References
https://gpdb.docs.pivotal.io/5280/utility_guide/admin_utilities/pgbouncer-admin.html

