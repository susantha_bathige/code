

## check the db properties
select datname,pg_catalog.pg_encoding_to_char(encoding) as "encoding" ,datcollate,datctype,pg_catalog.pg_get_userbyid(datdba) as "owner" from pg_catalog.pg_database;
datname | encoding | datcollate | datctype | owner

## How to restore postgres dump from s3:
#1. Download the file from s3. 
curl "https://xfrtu.s3.amazonaws.com/53c3d046-a36a-4d00-b72b-f0dab6fff3d0/2021-11-10T09%3A15%3A06Z/af52ebac-eb52-4187-bcad-f388477f3ebf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAQKF7VQWOLME46KGC%2F20211110%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20211110T160355Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=d3e23568fcf4c888cef8940fff9937b1bb3cd5294c4a6ec5bd07985b73080a3e" --output bv_stg.bump

## check / verify the options before using it. 
## some options may not be applicable and some options may need to add. Check the documentation
-j -> number-of-jobs
-e -> exit-on-error
-v -> verbose
pg_restore -U <username> -v--role ehrtutor --dbname learnwell --if-exists --clean -e -j 4 /tmp/<filename>

# restoring Board Vital DB
pg_restore -U ucnfbftrefdbh7 -v --role ucnfbftrefdbh7 --dbname d1uchr8lktdtps --if-exists --clean -j 4 boardvital_prod.dump

pg_restore -U nha_user -v --role nha_user --dbname nha_data --if-exists --clean -j 4 certportal.dump

# create the database with following syntax
createdb d1uchr8lktdtps -E UTF8 -l en_US.UTF-8 -T template0 -O ucnfbftrefdbh7

dropdb ${PGDATABASE}
createdb ${PGDATABASE}
time pg_restore \
    -U ucnfbftrefdbh7 \
    -j20 \
    -d d1uchr8lktdtps \
    --role ucnfbftrefdbh7 \
    --clean \
    --if-exists \
    boardvital_prod.dump 2>restore_010822.log


time vacuumdb -U susantha.bathige -d d1uchr8lktdtps -f -q

time vacuumdb -U susantha.bathige -d d1uchr8lktdtps -z -q

time reindexdb -U susantha.bathige -d d1uchr8lktdtps -j10 -q
--18 min


#!/bin/bash
d1=$(date '+%s')
sleep 10
d2=$(date '+%s')

diff=$(expr $d2 - $d1)
echo $diff

# use of variables in psql script
\set db_name d1uchr8lktdtps
create database :db_name;


## PITR
```bash

# get the time
psql -U postgres -Atc "select current_timestamp"

# to restore with PITR
sudo -u postgres pgbackrest --stanza=demo --delta \
       --type=time "--target=2021-12-31 19:57:03.172377+00" \
       --target-action=promote restore
```

# Start and stopping the service
sudo -u postgres pgbackrest stop

sudo -u postgres pgbackrest start
sudo -u postgres pgbackrest --stanza=demo start

# test restore
```text
https://pgbackrest.org/command.html
and don't forget --archive-mode=off in the restore command if that's a test restore
```

# kill a session
pg_terminate_backend(pid)

# Moving to the new DD
# create new stanza for the new dd
pgbackrest --stanza=boardvital --log-level-console=info stanza-create --repo2-path=/backups/datadomain_psql

# changes /etc/pgbackrest.conf on primary / standby
# add 
repo2-host
repo2-user

# make the script to take backups from repo2
pgbackrest --stanza=boardvital --log-level-console=info stanza-create --repo=2

# once everything is working fine remove the repo1 from /etc/pgbackrest.conf from primary / replicas

# How to make pg_restore faster
You can change below attributes before the restore and change them back to it's orginal values after the restore.
https://aws.amazon.com/blogs/database/best-practices-for-migrating-postgresql-databases-to-amazon-rds-and-amazon-aurora/
```text
maintenance_work_mem - higher value - You can reduce shared_buffer to give more memory to maintenance_work_mem
checkpoint_time - higher value
max_wal_size - higher value
```

