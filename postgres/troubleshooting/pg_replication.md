

```sql
-- check replication lag from standby
select now()-pg_last_xact_replay_timestamp() as replication_lag;

-- to see what is running
SELECT pid,leader_pid,datname, pid,usename,application_name, backend_start,
xact_start, query_start, current_timestamp - xact_start AS xact_runtime, state,wait_event_type,
backend_xmin,backend_type, query FROM pg_stat_activity WHERE state='active'
and usename not in ('susantha.bathige');

-- At primary
-- Replication lag
select
  pid,
  application_name,
  pg_wal_lsn_diff(pg_current_wal_lsn(), sent_lsn) sending_lag,
  pg_wal_lsn_diff(sent_lsn, flush_lsn) receiving_lag,
  pg_wal_lsn_diff(flush_lsn, replay_lsn) replaying_lag,
  pg_wal_lsn_diff(pg_current_wal_lsn(), replay_lsn) total_lag
from pg_stat_replication;
```

```bash
sudo systemctl statsu edb-as-13
```

