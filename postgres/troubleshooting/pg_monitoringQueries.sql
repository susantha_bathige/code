--Query: time since last analyze/autoanalyze
-- Query: time since last vacuum/autovacuum
-- time since last analyze/autoanalyze for the current database -- connect to each application database and execute the query
SELECT current_timestamp
, age(current_timestamp,max(last_analyze)) AS time_since_last_analyze
, age(current_timestamp,max(last_autoanalyze)) AS time_since_last_autoanalyze FROM pg_stat_all_tables;
-- time since last vacuum/autovacuum for the current database -- connect to each application database and execute the query
SELECT
current_timestamp
, age(current_timestamp,max(last_vacuum)) AS time_since_last_vacuum
, age(current_timestamp,max(last_autovacuum)) AS time_since_last_autovacuum FROM pg_stat_all_tables;
-- Query: table and index data
-- Show table size, total size of table plus indexes
-- last autovacuum and last autoanalyze for each table in a given list of schemas
select
pg_class.relname tablename,
pg_namespace.nspname schemaname,
pg_table_size(pg_namespace.nspname::text || '.' || pg_class.relname::text)/(1024*1024) table_size_Mb,
pg_total_relation_size(pg_namespace.nspname::text || '.' || pg_class.relname::text)/(1024*1024) table_and_inds_Mb,
pg_stat_all_tables.last_autovacuum,
pg_stat_all_tables.last_autoanalyze from
pg_class
join pg_namespace
on pg_class.relnamespace = pg_namespace.oid
join pg_stat_all_tables
on (pg_class.relname = pg_stat_all_tables.relname AND pg_namespace.nspname =
pg_stat_all_tables.schemaname) where
pg_namespace.nspname in ('myschema1','myschema2') order by tablename;

-- Queries: number of connections
-- Show number of active, inactive, total database connections
SELECT
now() as current_date,
count(pid) FILTER (WHERE state='active') AS "active sessions", count(pid) FILTER (WHERE state='inactive') AS "inactive sessions", count(pid) AS "total sessions"
FROM pg_stat_activity;
-- Show connections by database and by state
WITH mc AS
(SELECT current_setting('max_connections') AS max_connections)
, c AS
(SELECT nvl(datname,'background_process') AS database_name
, nvl(state,'background_process') AS connection_state , count(*) AS num_connections
FROM pg_stat_activity
GROUP BY datname
, state)
SELECT c.database_name
, c.connection_state
, c.num_connections
, mc.max_connections
, ROUND(100*c.num_connections/mc.max_connections::int) AS
percent_of_max_connections
FROM c JOIN mc ON TRUE
ORDER BY database_name, connection_state;

-- Show connections by state for all databases
WITH mc AS
(SELECT current_setting('max_connections') AS max_connections)
, c AS
(SELECT nvl(state,'background_process')
, count(*) AS num_connections FROM pg_stat_activity
GROUP BY state)
SELECT c.connection_state
, c.num_connections
, mc.max_connections
AS connection_state
, ROUND(100*c.num_connections/mc.max_connections::int) AS percent_of_max_connections
FROM c JOIN mc ON TRUE ORDER BY connection_state;

-- Show all connections
WITH mc AS (SELECT current_setting('max_connections') AS max_connections)
, c AS (SELECT count(*) AS total_connections FROM pg_stat_activity)
SELECT c.total_connections
, mc.max_connections
, ROUND(100*c.total_connections/mc.max_connections::int) AS percent_of_max_connections
FROM c JOIN mc ON TRUE;

-- Query: database size
-- Show size in Mb of scalocka and slk_audit databases
SELECT
now() as current_date,
pg_database_size('scalock')/(1024*1024) AS scalock_size_mb, pg_database_size('slk_audit')/(1024*1024) AS slk_audit_size_mb;
-- Query: frozen transaction ID
- the age (in transactions before the current transaction) -- of the database's frozen transaction ID
-- along with the percentage towards wraparound (2^32)
SELECT max(age_of_frozen_txid),
max(ROUND(100*(age_of_frozen_txid/2147483648::float))) AS percent_towards_wraparound FROM
(SELECT
datfrozenxid AS frozen_txid,
age(datfrozenxid) AS age_of_frozen_txid
FROM pg_database);


--check index bloat
WITH btree_index_atts AS (
    SELECT nspname, 
        indexclass.relname as index_name, 
        indexclass.reltuples, 
        indexclass.relpages, 
        indrelid, indexrelid,
        indexclass.relam,
        tableclass.relname as tablename,
        regexp_split_to_table(indkey::text, ' ')::smallint AS attnum,
        indexrelid as index_oid
    FROM pg_index
    JOIN pg_class AS indexclass ON pg_index.indexrelid = indexclass.oid
    JOIN pg_class AS tableclass ON pg_index.indrelid = tableclass.oid
    JOIN pg_namespace ON pg_namespace.oid = indexclass.relnamespace
    JOIN pg_am ON indexclass.relam = pg_am.oid
    WHERE pg_am.amname = 'btree' and indexclass.relpages > 0
         AND nspname NOT IN ('pg_catalog','information_schema')
    ),
index_item_sizes AS (
    SELECT
    ind_atts.nspname, ind_atts.index_name, 
    ind_atts.reltuples, ind_atts.relpages, ind_atts.relam,
    indrelid AS table_oid, index_oid,
    current_setting('block_size')::numeric AS bs,
    8 AS maxalign,
    24 AS pagehdr,
    CASE WHEN max(coalesce(pg_stats.null_frac,0)) = 0
        THEN 2
        ELSE 6
    END AS index_tuple_hdr,
    sum( (1-coalesce(pg_stats.null_frac, 0)) * coalesce(pg_stats.avg_width, 1024) ) AS nulldatawidth
    FROM pg_attribute
    JOIN btree_index_atts AS ind_atts ON pg_attribute.attrelid = ind_atts.indexrelid AND pg_attribute.attnum = ind_atts.attnum
    JOIN pg_stats ON pg_stats.schemaname = ind_atts.nspname
          -- stats for regular index columns
          AND ( (pg_stats.tablename = ind_atts.tablename AND pg_stats.attname = pg_catalog.pg_get_indexdef(pg_attribute.attrelid, pg_attribute.attnum, TRUE)) 
          -- stats for functional indexes
          OR   (pg_stats.tablename = ind_atts.index_name AND pg_stats.attname = pg_attribute.attname))
    WHERE pg_attribute.attnum > 0
    GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9
),
index_aligned_est AS (
    SELECT maxalign, bs, nspname, index_name, reltuples,
        relpages, relam, table_oid, index_oid,
        coalesce (
            ceil (
                reltuples * ( 6 
                    + maxalign 
                    - CASE
                        WHEN index_tuple_hdr%maxalign = 0 THEN maxalign
                        ELSE index_tuple_hdr%maxalign
                      END
                    + nulldatawidth 
                    + maxalign 
                    - CASE /* Add padding to the data to align on MAXALIGN */
                        WHEN nulldatawidth::integer%maxalign = 0 THEN maxalign
                        ELSE nulldatawidth::integer%maxalign
                      END
                )::numeric 
              / ( bs - pagehdr::NUMERIC )
              +1 )
         , 0 )
      as expected
    FROM index_item_sizes
),
raw_bloat AS (
    SELECT current_database() as dbname, nspname, pg_class.relname AS table_name, index_name,
        bs*(index_aligned_est.relpages)::bigint AS totalbytes, expected,
        CASE
            WHEN index_aligned_est.relpages <= expected 
                THEN 0
                ELSE bs*(index_aligned_est.relpages-expected)::bigint 
            END AS wastedbytes,
        CASE
            WHEN index_aligned_est.relpages <= expected
                THEN 0 
                ELSE bs*(index_aligned_est.relpages-expected)::bigint * 100 / (bs*(index_aligned_est.relpages)::bigint) 
            END AS realbloat,
        pg_relation_size(index_aligned_est.table_oid) as table_bytes,
        stat.idx_scan as index_scans
    FROM index_aligned_est
    JOIN pg_class ON pg_class.oid=index_aligned_est.table_oid
    JOIN pg_stat_user_indexes AS stat ON index_aligned_est.index_oid = stat.indexrelid
),
format_bloat AS (
SELECT dbname as database_name, nspname as schema_name, table_name, index_name,
        round(realbloat) as bloat_pct, round(wastedbytes/(1024^2)::NUMERIC) as bloat_mb,
        round(totalbytes/(1024^2)::NUMERIC,3) as index_mb,
        round(table_bytes/(1024^2)::NUMERIC,3) as table_mb,
        index_scans
FROM raw_bloat
)
-- final query outputting the bloated indexes
-- change the where and order by to change
-- what shows up as bloated
SELECT *
FROM format_bloat
WHERE ( bloat_pct > 50 and bloat_mb > 10 )
ORDER BY bloat_pct DESC;

-- Table bloat
--how much storage space dead tuples are using for each table/index
SELECT
  current_database(), schemaname, tablename, /*reltuples::bigint, relpages::bigint, otta,*/
  ROUND((CASE WHEN otta=0 THEN 0.0 ELSE sml.relpages::float/otta END)::numeric,1) AS tbloat,
  CASE WHEN relpages < otta THEN 0 ELSE bs*(sml.relpages-otta)::BIGINT END AS wastedbytes,
  iname, /*ituples::bigint, ipages::bigint, iotta,*/
  ROUND((CASE WHEN iotta=0 OR ipages=0 THEN 0.0 ELSE ipages::float/iotta END)::numeric,1) AS ibloat,
  CASE WHEN ipages < iotta THEN 0 ELSE bs*(ipages-iotta) END AS wastedibytes
FROM (
  SELECT
    schemaname, tablename, cc.reltuples, cc.relpages, bs,
    CEIL((cc.reltuples*((datahdr+ma-
      (CASE WHEN datahdr%ma=0 THEN ma ELSE datahdr%ma END))+nullhdr2+4))/(bs-20::float)) AS otta,
    COALESCE(c2.relname,'?') AS iname, COALESCE(c2.reltuples,0) AS ituples, COALESCE(c2.relpages,0) AS ipages,
    COALESCE(CEIL((c2.reltuples*(datahdr-12))/(bs-20::float)),0) AS iotta -- very rough approximation, assumes all cols
  FROM (
    SELECT
      ma,bs,schemaname,tablename,
      (datawidth+(hdr+ma-(case when hdr%ma=0 THEN ma ELSE hdr%ma END)))::numeric AS datahdr,
      (maxfracsum*(nullhdr+ma-(case when nullhdr%ma=0 THEN ma ELSE nullhdr%ma END))) AS nullhdr2
    FROM (
      SELECT
        schemaname, tablename, hdr, ma, bs,
        SUM((1-null_frac)*avg_width) AS datawidth,
        MAX(null_frac) AS maxfracsum,
        hdr+(
          SELECT 1+count(*)/8
          FROM pg_stats s2
          WHERE null_frac<>0 AND s2.schemaname = s.schemaname AND s2.tablename = s.tablename
        ) AS nullhdr
      FROM pg_stats s, (
        SELECT
          (SELECT current_setting('block_size')::numeric) AS bs,
          CASE WHEN substring(v,12,3) IN ('8.0','8.1','8.2') THEN 27 ELSE 23 END AS hdr,
          CASE WHEN v ~ 'mingw32' THEN 8 ELSE 4 END AS ma
        FROM (SELECT version() AS v) AS foo
      ) AS constants
      GROUP BY 1,2,3,4,5
    ) AS foo
  ) AS rs
  JOIN pg_class cc ON cc.relname = rs.tablename
  JOIN pg_namespace nn ON cc.relnamespace = nn.oid AND nn.nspname = rs.schemaname AND nn.nspname <> 'information_schema'
  LEFT JOIN pg_index i ON indrelid = cc.oid
  LEFT JOIN pg_class c2 ON c2.oid = i.indexrelid
) AS sml
ORDER BY wastedbytes DESC

-- Index creation progress
SELECT 
	round(p.blocks_done::numeric / p.blocks_total::numeric * 100, 2) AS pct, 
	age(CURRENT_TIMESTAMP, a.query_start) AS runtime,
	p.blocks_total::numeric / EXTRACT(EPOCH FROM age(CURRENT_TIMESTAMP, a.query_start)) AS blocks_per_s,
	(p.blocks_total - p.blocks_done) AS blocks_remaining,
	(p.blocks_total - p.blocks_done)::numeric / (p.blocks_total::numeric / EXTRACT(EPOCH FROM age(CURRENT_TIMESTAMP, a.query_start))) AS s_remaining,
	(CURRENT_TIMESTAMP + ('1 second'::interval * (p.blocks_total - p.blocks_done) / (p.blocks_total::numeric / EXTRACT(EPOCH FROM age(CURRENT_TIMESTAMP, a.query_start))))) AT TIME ZONE 'America/Chicago' AS est_complete,
	p.phase,
	a.query
FROM pg_stat_progress_create_index p
JOIN pg_stat_activity a
	ON a.pid = p.pid
;


-- look at the age of the oldest snapshots that are running
SELECT
	now() - CASE
		WHEN backend_xid IS NOT NULL THEN xact_start
		ELSE query_start
	END AS age,
	pid,
	backend_xid AS xid,
	backend_xmin AS xmin,
	STATE
FROM
	pg_stat_activity
WHERE
	backend_type = 'client backend'
ORDER BY
	1 DESC;

-- catching longest running query
SELECT
	current_timestamp - query_start AS runtime,
	datname,
	usename,
	substring(query, 0, 100) as query
FROM
	pg_stat_activity
WHERE
	STATE = 'active'
ORDER BY
	1 DESC;

-- queries which are blocked
SELECT
	datname,
	usename,
	wait_event_type,
	wait_event,
	backend_type,
	query
FROM
	pg_stat_activity
WHERE
	wait_event_type IS NOT NULL
	AND wait_event_type NOT IN ('Activity', 'Client');

-- who is blocking
SELECT
	datname,
	usename,
	wait_event_type,
	wait_event,
	pg_blocking_pids(pid) AS blocked_by,
	backend_type,
	query
FROM
	pg_stat_activity
WHERE
	wait_event_type IS NOT NULL 
	
-- kill idle connections
-- Execute with care
SELECT
	--pg_terminate_backend(pid)
FROM
	pg_stat_activity
WHERE
	STATE = 'idle in transaction'
	AND current_timestamp – state_change > '10 min';

-- Queries running more than 5 min
SELECT
	pid,
	datname,
	usename,
	application_name,
	client_addr,
	client_hostname,
	wait_event_type,
	now() - pg_stat_activity.query_start AS duration,
	state
FROM
	pg_stat_activity
WHERE
	(now() - pg_stat_activity.query_start) > interval '5 minutes';

--estimated row count of a table
--891,822,700
SELECT reltuples AS estimate FROM pg_class where relname = 'responses'; 
