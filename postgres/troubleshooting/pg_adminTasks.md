
# Rebuild indexes
SET max_parallel_maintenance_workers TO 4;
SET maintenance_work_mem TO '4 GB'

reindex index concurrently index_responses_on_quiz_id_and_question_id;

# check all the processes runing with perticular PID. 
 \top -b -d 1 -n 3 | grep 5150

