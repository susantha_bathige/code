
# list of publications
select * from pg_publication;

# how to get tables in publications
select * from pg_publication_tables;

# get changes
SELECT *
FROM pg_logical_slot_get_changes('bv_cdc_replslotqa', NULL, NULL) limit 10;

SELECT * FROM pg_logical_slot_peek_changes('bv_cdc_replslotqa', NULL, NULL) limit 10;

# replication lag
--replication lag in Mb.
SELECT slot_name,
       pg_size_pretty(pg_current_wal_lsn() - confirmed_flush_lsn) AS replication_lag
FROM pg_replication_slots;