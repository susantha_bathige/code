
# settings that requires a restart
```sql
select name, setting, category, context, pending_restart from pg_settings
where context = 'internal';
```
# Meaning of Context field
```text
internal – can't really be changed
postmaster – require restart
sighup, superuser-backend, backend, superuser, and user – will get loaded on reload
```

# What level we cat set postgres configs
We can change parameters for specific user, for specific database, or even, for specific user in specific database.

For example, in my DB, I have set, globally, log_min_duration_statement to 0, with this in place, I'll run:
```sql
alter user depesz set log_min_duration_statement = 10;
alter database depesz set log_min_duration_statement = 20;
alter user pgdba in database pgdba set log_min_duration_statement = 30;
-- to revert, you can use 
alter system reset
alter user reset
alter database reset 
alter user ... in database ... reset
reset log_min_duration_statement
```

# metadata views
select * from pg_db_role_setting;

# Links
# how, when and where to set postgresql configs
https://www.depesz.com/2021/03/01/starting-with-pg-where-how-can-i-set-configuration-parameters/
