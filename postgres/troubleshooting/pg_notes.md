
# short cut for Explain, Analyse
\set eab 'explain (analyse, buffers)'
:eab <query>

# recursive query to get DISTINCT value
```sql
WITH RECURSIVE cte AS (
   (SELECT tags_id FROM cpu ORDER BY tags_id, time DESC LIMIT 1)
   UNION ALL
   SELECT (
      SELECT tags_id FROM cpu
      WHERE tags_id > t.tags_id 
      ORDER BY tags_id, time DESC LIMIT 1
   )
   FROM cte t
   WHERE t.tags_id IS NOT NULL
)
SELECT * FROM cte LIMIT 50;
```

# Create a user
```sql
create role developer_read;

grant usage on schema public to developer_read;
grant select on all tables in schema public to developer_read;

create user "Manisha.Poptani" with password 'D.]Qd7RH';
grant developer_read to "Manisha.Poptani";
```

# Copy data to a csv file
psql -U postgres d1uchr8lktdtps -c "\copy (SELECT * FROM responses) TO '/pgwal_archive/responses.csv' WITH (FORMAT CSV, DELIMITER ',')"
\copy (SELECT * FROM responses) TO '/pgwal_archive/responses.csv' WITH (FORMAT CSV, DELIMITER ',')
\copy (SELECT * FROM pg_stat_activity) TO '/pgwal_archive/data/test.csv' WITH (FORMAT CSV, DELIMITER ',')

copy responses to program 'gzip > /pgwal_archive/data/responses.csv.gz' with (format csv, header, delimiter ',');

psql -U postgres -c "\copy (SELECT * FROM pg_stat_activity) TO '/pgwal_archive/data/test.csv' WITH (FORMAT CSV, DELIMITER ',')"