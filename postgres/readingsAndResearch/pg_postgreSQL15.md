
# New Features

> DBA and administration

- pg_stat_statements
  - I/O timing for temp files
  - JIT counters
- New wait events
  - AcrhiveCommand
  - ArchiveCleanupCommand
  - RestoreCommand
  - RecoveryEndCommand
- Logging changes
  - Startup process logs what it's doing
    - every log_startup_progress_interval (10s)
  - New defaults
    - log_autovacuum_min_duration = 10 min
    - log_checkpoints = on 
- JSON logging
  - log_destination = jsonlog
  - like csvlog
    - But json
    - always written to file
- Security invoker views
  - check permissions with callers privilleges
  - default: check with view creators
  - 
> SQL and developer
- 
> Backup and replication
> Peformance
> Breaking changes

- Python 2 support dropped
- public schema no longer has create permissions. Previous versions any user can create objects in public schema. 
  - only pg_database_owner
- public retains usage permissions
- Exclusive backup mode (advice is to use one of the ready-made backup tools such as pgbackrest)
  - removed
    - unsafe
  - use non-exclusive mode
    - or pg_basebackup
- Allow root-owned SSL private keysin libpq
  - already allowed in backend
- Predefined roles
  - pg_checkpoint (allow to run checkpoint)
   
- Permissions on GUCs (Grand Unified Configuration)
  - reduce permissions on superuser gucs
- 

