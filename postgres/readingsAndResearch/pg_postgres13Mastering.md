
# Generate random UUID
select gen_random_uuid();

# Drop a database faster. Even users are connected to the db
DROP DATABASE test WITH (force);

# Persisted/Stored columns
create table t_test (
    a int,
    b int,
    c int GENERATED ALWAYS AS (a * b) STORED
);
insert into t_test (a ,b) values (10,20);
    # get rid of the stored column expression but not the column
    alter table t_test alter column c drop expression;
    \d t_test;

# Deduplication of B-tree indexes
Indexes on columns which has many duplicate values is much more smaller in size in PostgreSQL 13. 
This makes less IO. Consider recreating indexes when migrating from PostgreSQL 12.

# Incremental Sorting
New performance improvement. Need to read this further. 

# Reindexdb with multi-threaded
time reindexdb -j 8 database_name

# Aggregate
Group Aggregate - Large number of groups
Hash Aggregate - Small number of groupd (done on memory)

# Parallelizing VACUUM operations
    # simply mention how many cores you want to use for the vacuum operation. 
VACUUM (verbose ON, analyse ON, parallel 4) some_table;

# track pg_basebackup progress
\d pg_stat_progress_basebackup

# track analyze progress
\d pg_stat_progress_analyze

# pg_stat_replication
can track spill operation to disk

# track allocated shared memory
\d pg_shmem_allocations;

# pg_stat_slru
Need to read more

# get real time
clock_timestamp()
now() -> not a real time

# transactions
BEGIN -> start a trasnaction
COMMIT -> end a transaction
    COMMIT WORK
    COMMIT TRANSACTION
    END
ROLLBACK -> counterpart of commit
    ABORT

# PostgreSQL supports Transactional DDL. 

# update 1 table insert into another table in a single statement
    # this technique can be used to generate invoice # like values with no gap.
with x as (update t_watermark set id = id + 1 returning *)
insert into t_invoice
select * from x returning *;

# SELECT FOR UPDATE NOWAIT;
    # another session will not wait until one update finishes. 
    # also you can use lock_timeout setting at session level
        e.g: set lock_timeout to 5000; -> 5 sec

# SELECT FOR UPDATE SKIP LOCKED
    # so multiple users can book without waiting.
    select * from t_flight limit 2
    postgresql13-*# for update skip locked;
    
# FOR UPDATE have an impact on FKs.
    Even the table we update is child table, if a column is linked to parent table (FK),
    anu update on parent table is also will be blocked. 


# Isolation Levels
    Specially useful if you need to run reports.

# Advisory locks
    Specify just a number
select pg_advisory_lock(15);
select pg_advisory_unlock_all();

# VACUUM
    # autovacuum tool - to remove dead tuples
        - works in the background
        - lots of parameters to control the behavior of autovacuum
        - is a key to performance
        - vacuum on large tables can be expensive
        - VACUUM FULL - NEVER do it in prod bc it will lock the table

    # vacuum process will take care optimizer stats as well (analyze)

    # attributes
        - autovacuum_naptime
        - autovacuum_max_workers
        - autovacuum_vacuum_thredhold = 50 ( at least 50 rows, otherwise for small tables 20% will reach with very few modifications.
            not worth of folking an entire process to vacuum)
        - autovacuum_analyze_threshold = 50
        - autovacuum_vacuum_scale_factor = 0.2 (table is worth vacuuming if 20% of table data has been changed)
        - autovacuum_analyse_scale_factor = 0.1 
        - autovacuum_vacuum_insert_threshold = 1000 (pg13 will trigger autovacuum even if only insert stmts are goin on)

        # there are lot of attributes to autovacuum as well as manual vacuum
    # You can create a table with autovacuum settings;
        create table t_test (id int) with (autovacuum_enabled = off); - make sense for tables with short life cycle
    
# Transaction wrap around
    How transaction IDs are manage and cycle. Trans IDs are finite, not unlimitted. At some point they will start to wrap around. 
    To manage Transaction IDs, vacuum is needed. Not all transactions will increase the xact ID. As long as transactions is still reading, it will only 
        have a virtual xact ID. 
    - autovacuum_freeze_max_age = 200,000,000 (defined max no.of transactions that pg_class.relfrozenxid field can attain before a VACUUM)
    - autovacuum_multixact_freeze_max_age = 400,000,000

# Limitting trasnactions by making use of snapshot too old
    - really long transaction can postpone clean up for quite sometme. The logical consequnce was table bloat. So performance go downhill.
    - administrator can intelligently limit the durtation of the transaction by using the setting below:
        old_snapshot_threshold = -1 (disable) instance wide setting not applicable in session wise
        you can limit the long running transactions by setting this attribute. 

# More on VACUUM
    - vacuum can skip pages bc visibility maps suggest that a block is visible to everyone. 
    - DISABLE_PAGE_SKIIPING disables that behavior and ensures that all pages are cleaned during this run.
    - SKIP_LOCKED - idea is to make sure vacuum does not harm concurrency. vacuum will automatically skip over relations which can not instantly be locked. 
      This feature is very helpful in the event of heavy concurrency. 
    - vacuum is also taken care of indexes. You can change it using INDEX_CLEANUP false

# Making use of indexes
    - bad indexing is the main cause of bad performance. 
    - There is no replacement for missing indexes. 
    - query execution stages (parser, rewrite, optimizer, plan)

    # EXPLAIN (analyse true, buffers true, timing true)
    - set the parallel workers for a query;
        set max_parallel_workers_per_gather = 0;
    - Seq Scan - reading the entire table (table scan). It can go parallel as well. Parallel Seq Scan
    - Cost can't be trasnlate to milliseconds.It is just an estimates in terms of penalty points. 
    - min_parellel_tables_scan_size = 8 MB (min size of a table that's considered for parallel queries) 
      min_parellel_index_scan_size = 512 KB
    - can build indexes concurrently
    - max_parellel_maintenance_workers - upper limit of worker processes
    - Index can be used to speed up these operations, bc they need sorted outpur
        * order by
        * min/max
    - query with even one table can make use of multiple indexes
    - Bitmap Index Scan -(similar to index seek in SQL Server)
    - Bitmap heap scans are used by comparatively expensive queries
    - if the no.of rows is smaller, Postgres will again consider bitmap scans and normal index scans
    - Eexecution plans are dependent on input values
    - pg_stats is a system vuew containing all the statistics about the content of the columns. 


    # Clustering tables
    - CLUSTER command allows us to rewrite a table in the desired order. 
    - CLUSTER command will lock the table
        CLUSTER t_random using idx_random;

    # Index only scans
    - index scan / index only scan (significantly better performance)
      PostgreSQL 11 and onwards supports INCLUDE keyword like MS SQL 


    # Functional Indexes
    ---------------------
    - Instead of indexing the value the output of a function is stored in the index
    - Partial indexes (filtered indexes) can be used to reduce the index size. It only make sense to exclude very frequent values that makes up a large
      part of the table (at least 25% or so). It need to some deep knowledge of the data. 

    # Creating indexes concurrently to avoid blockings
      - CREATE INDEX CONCURRENTLY (it takes sometime but will not lock the table). Similar to the WITH ONLINE = ON in MS SQL.
      - Sometime postgres will not create the index and will marked it as invalid. So check \d <table name> to verify

    # Operator classes
    ------------------
    - Applied for special values which needs non-standard sort order or non-standard operator behavior. 
    - Further reading is necessary
    - forcing sql scan 
    set enable_seqscan to off


    # Index Types
    -------------
    - PostgreSQL has many index types 
    - Index types of postgres
      select * from pg_am;
    
    # Fuzzy Searching
    -----------------
    - CREATE EXTENSION pg_trgm;
    - You can import data from text file in internet
      copy t_location from program 'curl https://www.cybertec-postgresql.com/secret/orte.txt';
    - Distance operator - Some examples below:
      select 'abcde' <-> 'abcdeacb'; -> the lower the number, the more similar the two strings are
      select * from t_location order by name <-> 'kramertneusiedel' limit 3; - the more closet match will be the top record
      create index idx_trgm on t_location using GiST(name GiST_trgm_ops);

    # Speeding up LIKE queries
    --------------------------
    - The trigram index that is used above can be also used to speed up LIKE queries

    # Handling Regular Expressions
    ------------------------------
    - Again, trigram indexes can be used:
      - select * from t_location where name ~ '[A-C].*neu.*';

    # Full-Text Search
    ------------------
    - Further reading requires


# CHAPTER 3 - Handling Advanced SQL 

## Grouping Sets

> rollup (additional line with overral avg)
```sql
select region, avg(production)
from t_oil
group by rollup (region);
```
> cube (all possible combination of groups)
```sql
select region, country, avg(production)
from t_oil
where country IN ('USA','Canada','Iran','Oman')
group by CUBE (region, country);
```

> grouping sets ( you can explicitly list the aggregates you want)
```sql
group by GROUPING SETS ((), region, country);
```
> Internally postgres uses MixedAggregate to perform the aggregation. You can force GroupAggregate if you disable the HashAggregate.
```pgsql
set enable_hashagg to off;
```
> Grouping Sets with FILTER caluse.
  Filter is faster than CASE in select stmt. 
```sql
select region,
    avg(production) as all,
    avg(production) FILTER (WHERE year < 1990) as old,
    avg(production) FILTER (WHERE year >= 1990) as new
from t_oil
```

## Ordered Sets
> Data is grouped normally, and then data inside each group is ordered given a certain condition. The percentile_disc function will skip 50% of the group and return the desired value.
```sql
select region, percentile_disc(0.5) WITHIN GROUP (ORDER BY production)
from t_oil 
group by 1;
```
> Can use ordered sets along with grouping sets. 

> There are more to grouping sets. 

## Window Functions And Analytics
>> The OVER clause defines the window we're looking for. 

> Compare the current row to the overral avg. Note, there is no GROUP BY here but
> OVER (). 
```sql
select country, year, production, 
    consumption, avg(production) OVER ()
from t_oil limit 4;
```
> Partitioning data. Note, here the avg is country wise not the overrall avg.
```sql
select country, year, production, consumption,
    avg(production) OVER (PARTITION BY country)
from t_oil;

--partition with a condition, year < 1990 returns true or false internally.
select year, production,
    avg(production) OVER (PARTITION BY year < 1990)
from t_oil
where country = 'Canada'
order by year;

--sort data inside a window
OVER (PARTITION BY country ORDER BY year)
```
>> Sliding Windows

> This is to calculate moving avg.
```sql
/*
Moving window should be used within an ORDER BY
ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING defines the window
the current row, the one before it, and the one after the current row
*/
select country, year, production,
    min(production)
    OVER (PARTITION BY country
    ORDER BY year ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING)
from t_oil
where year BETWEEN 1978 and 1983
    and country IN ('Iran', 'Oman');

--how sliding window works. array_agg will turn a list of values to PostgreSQL array
select *, array_agg(id)
    OVER (ORDER BY id ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING)
from generate_series(1, 5) as id;
```
> There are more to sliding window. 
#
# Chapter 5: Log Files and System Statistics
- Monitoring is the key to running database professionally
>> System Views

> pg_stat_activity - checking live traffic. This is similar to sys.dm_exec_requests in MS SQL. 
  
  - set application_name to 'something'; - dev can set this from the app. 
```sql
select pid, query_start, state_change, state, query
from pg_stat_activity;
/*
How to get rid of the bad query
pg_cancel_backend - cancel the query but leave the connection in place
pg_terminate_backend - kill the entire db connection
*/

--terminate all other users but you
select pg_terminate_backend(pid)
    from pg_stat_activity
    where pid <> pg_backend_pid()
        and backend_type = 'client backend';

--database level stats
/*
\d pg_stat_database - returns one line per db
    numbackends - no.of db connections that are currently open
    tup_columns - there is lot of reading or writing goin on
    temp_files/ temp_bytes - whether db has to write temp files to disk which will slow down operations.
*/
```
>  Reasons for high temp files
  
    - Poor settings - e.g: work_mem is too low
    - Stupid operations - expensive queries
    - Indexing and other admin tasks - index creation or DDLs. But not necessarily considered a problem

> Measuring IO timing - /usr/lib/postgresql/13/bin/pg_test_timing 
  
  - four-digits value will slow down the system
  - 

> Inspecting tables

- Two system tables are useful to get info at table level:
    - pg_stat_user_tables (one entry for each table) 
      - seq_scan - no.of seq scans
      - seq_tup_read - how many tuples has to read during those seq_scans
      - idx_scan - how aften index was used
      - n_tup_ - how much inserted, updated and deleted
    - pg_statio_user_tables - caching behavior of the tables

```sql
--which tables may need an index
--find large tables that have been used frequently in seq scan
--This query can identify missing indexes
select schemaname, relname, seq_scan, seq_tup_read,
    seq_tup_read / seq_scan as avg, idx_scan
from pg_stat_user_tables
where seq_scan > 0
order by seq_tup_read desc limit 25;
```

> Diging into indexes

- \d pg_stat_user_indexes - find pointless indexes
- \d pg_statio_user_indexes - contains caching info about an index
  
```sql
--potential unusage indexes
select schemaname, relname, indexrelname, idx_scan, 
       pg_size_pretty(pg_relation_size(indexrelid)) AS idx_size,
       pg_size_pretty(sum(pg_relation_size(indexrelid)) 
               OVER (ORDER BY idx_scan, indexrelid)) AS total 
from   pg_stat_user_indexes 
order by 6 ;
```
> Background worker stats

- \d pg_stat_bgwriter 
  - checkpoints_req - if high this mean that a lot of data has been written and the checkpoints are always triggered because of high throughput
  - Requested checkpoints always triggers because of the high throughput
  - checkpoint_write_time - time needed to write the checkpoint
  - checkpoint_sync_time - time needed to sync 
  - buffers_checkpoint - how many buffers were written during the checkpoint
  - buffers_clean - how many were written by the bg writer
  - maxwritten_clean - no.of times bg writer stopped a cleaning scan because it had written too many buffers
  - buffers_backend - no.of buffers directly written by the backend db connection
  - buffers_backend_fsync - buffers flush by db connection

> Tracking archiving, and streaming

Features related to replication and transaction log archiving.

- \d pg_stat_archiver (keep an eye on below columns)
  - failed_clount 
  - last_failed_count
- \d pg_stat_replication (if replication configured) - one entry per WAL sender process. You can query this from the sending server.
  - sync_state - state of the other side of the streaming is
- \d pg_stat_wal_receiver 
  - received_lsn - latest WAL position
  - received_tli - latest WAL time line
  - latest_end_lsn - last WAL position that was reported to the WAL sender

> SSL Connections

- \d pg_stat_ssl

> Inspecting individual transactions

- \d pg_stat_xact_user_tables - Identical to pg_stat_user_tables but the context is different.

> Tracking VACUUM and CREATE INDEX progress

- \d pg_stat_progress_vacuum (vacuum is pretty fast so that progress can be rapid and hard to track)
- \d pg_stat_progress_create_index 
  - tuples_total 
  - tuples_done

- \d pg_stat_statements (Separate extension). Provides one line per query. Same query which uses different parameters are aggregated. 
  - min_time, max_time, mean_time, std_dev
  - std deviation is sepcially important because it will tell us whether a query has stable or fluctuating runtimes. Unstable runtimes can occur for various reasons:
    - data is not fully cached -> have to fetch from disk
    - parameter sniffing can create inefficient plans
    - concurrency and locking
    - skewed data distribution
  -  shared_cloumns_* - how many blocks from cache (_hit) or from the disk (_read)
  -  provides info about temp IO. High temp IO can be due to following reasons:
     -  large DDL activities
     -  low work_mem (OLTP)
     -  suboptimal maintenance_work_mem settings (OLTP)
     -  queries that should not have run in the first place
  -  Never run SELECT * on this view
  -  It cuts off queries at 1024 bytes. Consider increasing this to 16.384.
     -  show track_activity_query_szie;
```sql
-- top 10 queries with their run time
SELECT     round((100 * total_exec_time / sum(total_exec_time) 
                    OVER ())::numeric, 2) percent, 
                  round(total_exec_time::numeric, 2) AS total, 
                  calls,
            round(mean_exec_time::numeric, 2) AS mean, 
            substring(query, 1, 40) 
FROM      pg_stat_statements 
ORDER BY total_exec_time DESC 
LIMIT 10;
```
- select pg_stat_statements_reset() - resetting this once in while makes lot of sense

> Configure log file (postgresql.conf)
- Logging - stderr (Unix) and trun the logging collector on
  - show log_destination;
  - show logging_collector;
  - show log_directory; (where to store the log file)
  - show log_filename;
  - show log_truncate_on_rotation;
  - show log_rotation_age;
  - show log_rotation_size;

> Logging slow queries (-1 means dsiable)

- show og_min_duration_statement

> What to log

By default only errors will be logged. Logging is key to seeing what is going on in the database. However, it is recommended configuring logging wisely to avoid excessive log production and thus slowdowns. 
- show log_checkpoints;
- show log_connections; (for OLTP systems, it creates extensive logging)
- show log_disconnections;
- show log_duration; 
- show log_line_prefix; (adds more details to the logging)
- show log_lock_waits;
- show log_statement; (what to log? by default errors - none)
- show log_replication_commands;
- show log_temp_files; (4 MB is a good start)
- show log_timezone; (comman TZ is useful if servers are geo spreaded)

> Control the enormous amounts of logs

The idea is to reduce the no.of logs while still maintaining the usefulness of those settings as such.
- show log_min_duration_sample;
- show log_statement_sample_rate;
- show log_transaction_sample_rate;

#
# Chapter 6: Optimizing Queries for Good Performance

> Joining tables
- Nested loops - are often used if one of tthe sides is very small and contains only limited set of data. For large data sets, this is not an option. 
```
for x in table1:
    for y in table2:
        if x.field = y.field 
            issue row
        else
            keep going
``` 
- Hash joins - both sides can be hashed and hash keys could be compared. Hash values need to be stored in memory as hash tables. 
```
Hash join
    Sequential scan table 1
    Sequential scan table 2
```
- Merge joins - use sorted lists to join the results in both sides. It can handle a lot of data. 
```
-- PostgreSQL will sort the data before the join
Merge join
    Sort table 1
        Sequential scan table 1
    Sort table 2
        Sequential scan table 2

-- already sorted data
Merge join
    Index scan table 1
    Index scan table 2
```
- All of the above joins are available in parallel versions too. 
- Do not use OFFSET 0 in views

> EXPLAIN with more verbosity

EXPLAIN (analyse, verbose, costs, timing, buffers)

> Spotting problems in Exec Plans

If the query is slow where does the run time jumps.
- Estimates and real numbers should reasonably close together
- Look for functional indexes
  - create index idx_cosine on t_estimate (cos(id)); 
- Consider cross-column correlation. The fact that, PostgreSQL keeps column stats that do not span more than one column can lead to bad results. Multivariate stats is the solution to this from PosttgreSQL 10 and onwards. 
- Check whether all outer joins are really neccessary and also check the semantics of such joins
- work_mem - adequate setting requires to speed up group by and sorting operations

> Mechanisms for sorting
- merge disk
- quick sort
- top-N heap sort - used to provide only top N rows
- incremental sort 

The admin tasks such as CREATE INDEX clause don't reply on the work_mem and use maintenance_work_mem instead. 

> Parallel Queries

- PostgreSQL is now capable to build btree indexes in parallel. 
show max_parallel_maintenance_workers
- show max_parallel_workers_per_gather - limits the no.of cores
- show min_parallel_table_scan_size
- you can also set the parallel workers at table level 
  - alter table t_parallel set (parallel_workers = 9);
- show max_worker_processes; - how many workers generally available
- show max_parallel_workers; - how many workers available for parallel queries
  
Note, more cores doesn't automatically lead to more speed. 

#
# Chapter 7: Writing Stored Procedures

#
# Chapter 8: Managing PostgreSQL Security

> Security hieacrhy 
- Bind adreesess (postgresql.conf)
  - By default postgres does not accept remote connections. It does not even reject connections, it simply does not listen on port
  - listen_address - put the server IP in here not the client ip
  - connection related settings requires postgres restrat
  - nc -vz <ip> 5432 - check whether postgre server listening on the port
- Host-based access control (pg_hba.conf)
  - trust - no password needed to access
  - scram-sha-256 is better over md5
  - peer - Unix users
  - \d pg_stat_ssl - check for ssl connections
- Instance-level permissions
  - users and roles are same except users get the login attribute
  - Creator is automatically the owner of the object
  - Streaming replication user should not be a superuser (security POV)
  - using ALTER ROLE to change the pwd is not a good idea as it log the pwd in plaintext
- Database-level permissions
  - CREATE does not allow user to create tables instead SCHEMA
  - CONNECT - allows user to connect to a db
  - REVOKE ALL ON DATABASE test FROM public; -- otherwise any user can connect to any db
  - GRANT CONNECT ON DATABASE test TO bookkeepr; -- explicity grant 
- Schema-level permissions
  - By default public will allow to work with public schema. So that even new user can create a table in public schema
  - All permissions are automatically available to the creator including the drop
  - REVOKE ALL ON SCHEMA public FROM public; -- take permissions away from public
  - SHOW search_path; -- by default postgres will try to put the object here
  - CREATE - on schema means, user can put objects into a schema
  - GRANT USAGE ON SCHHEMA public to bookkeeper;
    - USAGE - user is allowed to enter the schema. That does not mean, a user can use an object inside the schema
  - GRANT CREATE ON SCHEMA public TO bookkeeper;
- Table-level permissions
  - GRANT can be used to set permissions on all the tables in a schema at the same time
- Column-level permissions
- RLS - restricting access to rows


> Configuring default privillages
- ALTER DEFAULT PRIVILLEGES FOR ROLE joe 
      IN SCHEMA public GRANT ALL ON TABLES TO pual;
- \z t_person -> see the privilleges

#
# Chapter 9: Handling Backup and Recovery

> Peforming simple backups 
```bash
# output will be sent to stdout which gives more flexibility in linus
pg_dump test > /tmp/dump.sql

# dump entire instance
pg_dumpall > /tmp/dump_all.sql
```
- Using custom format (Fc) is a good idea as the backup will shrink in size
- Set env variables 
  - export PGHOST=localhost
  - export PGUSER=hs
  - export PGPASSWORD=abc
  - export PGPORT=5432
  - export PGDATABASE=test

Then simply use `psql or pg_dump` to connect. All applications based on standard PostgreSQL C language client library(libpg) will understand these environment variables.

- .pgpass
- service files (.pg_service.conf)
- Inspect the backup 
```bash
pg_restore --list /tmp/dmup.fc
```

> Replaying backups
- You have multiple options
```bash
# plaintext backup
psql test < backup.sql

# pg_restore
pg_restore -d new_db -j 4 /tmp/dump.fc
```

#
# Chapter 10: Making Sense of Backups and Replication

> typically the WAL directory consists of 16MB files. This can be changed with initdb. In some cases, this can speed up things. 
```bash
initdb -D /pgdata --wal-segsize=32
```
- WAL is written in binary format
- checkpoint - happens automatically in background and it will recyle the WAL t-log
- Never ever delete t-log files manually

> Optimizing the transaction log
- There are config settings that decide when a checkpoint is initiated
  - checkpoint_timeout = 5min (time between two checkpoints)
  - max_wal_size = 1GB (soft limit)
  - min_wal_size = 80MB
- The amount of space provided to store the t-logs will vary between the min_wal_size and max_wal_size. 
- larger checkpoint distances will lead to less WAL ?????
- checkpoint_completion_target = 0.5 (increasing this value means that the checkpoint is stretched out and less intensive)

> Archiving and recovery

Check the docs on how to setup the wal archiving.
- wal_level = replica (default)
- max_wal_senders = 10
- archive_mode = on
- archive_command = 'cp %p /archive/%f' ( we can put any command here such as rsync, scp, etc) - this will transport the wal files to a safe location.

Configuring pg_hba.conf
- This is neccessary only if we're planning to use pg_basebackup.
- Create initial backup (base backup)
```bash
pg_basebackup -D /some_traget_sir
    -h localhost (master server ip)
    --checkpoint=fast
    --wal-method=stream
```
- \d pg_stat_archiver - check archive status

> Replaying the transaction log
```bash
restore_command = 'cp /archive/%f %p'
recovery_target_time = '2020-10-02 12:42:00' # for PITR
recovery-target_action = 'pause' # no promotion after the replay
recovery_target_name = 'my_daily_process_ended' # restore up to the recovery point
```
- \df *pause* - check all the matching functions

> Setting up asynchronous replication
```bash
ps ax | grep sender # check wal sender process at primary
```
```sql
-- create dedicated user for replication
-- do not user superuser account for replication
CREATE USER repl LOGIN REPLICATION
```
```sql
-- to pause the replication. Execute this on the stanby
SELECT pg_wal_replay_pause(); 
-- resume the replication
SELECT pg_wal_replay_resume();
```
- \d pg_stat_wal_receiver; # repl stats on the receiving side
- show max_standby_streaming_delay;
- show hot_standby_feedback_off;

> Making replication more reliable
- show wal_keep_size;

> Synchronous replication
- show synchronous_standby_names;
- show application_name;

#
# Chapter 11: Deciding on Useful Extensions

- \d pg_available_extensions;
- \d pg_available_extension_versions;
- create extension adminpack;
- create extension bloom;
  
You can search by using any columns if you create a bloom index.
```sql
create index idx_bloom2 on t_bloom2
   using bloom(col1, col2, col3, col4, col5, col6, col7, col8, col9);
```
But the database has to read the entire bloom filter in order to use it. Because of that, we can use bloom filters always. 

- deploying btree_gist and btree_gin indexes
```sql
create extension btree_gist;
create extension btree_gin;
```

- Fetching files with file_fdw
```sql
create extension file_fdw;
```
Sometime it can make sense to read a file from disk and expose it to PostgreSQL ad a table. 

- Inspecting storage with pgstattuple
```sql
create extension pgstattuple;
```

> There is more to this chapter

#
# Chapter 12: Troubleshooting PostgreSQL 
Need to apply the systematic approach.

- Approaching an unknown database
  
  Need to have data. Which means need to have proper monitoring setup. 

- Inspecting pg_stat_activity
  
  Check the above view to answer the following questions;
  - how many concurrent queries are currently being executed on your system?
  - do you see similar type of queries showing up in the query column all the time?
  - do you queries that have been running for a long time?
  - Are there any locks that have not been granted?
  - do you see connections from suspicious hosts?

```sql
-- get overrall look
/*
look for high ilde_in_trans - dig deeper
*/
select datname, count(*) as open,
   count(*) filter (where state = 'active' ) as active,
   count(*) filter (where state = 'idle' ) as idle,
   count(*) filter (where state = 'idle in transaction' ) as idle_in_trans
from pg_stat_activity
where backend_type = 'client backend'
group by rollup(1);

-- how long transactions are open:
/*
In most apps, trans that takes so long is highly suspicios and highly dangerous.
Vacuum cmd can only clean up dead rows if no transaction ca see them anymore. If transaction stays open, vacuum cmd can't produce useful results, which will lead to table bloat.
*/
select pid, xact_start, now() - xact_start as duration
  from pg_stat_activity
where state like '%transaction%'
order by 3 desc;

-- check for long running queries
/*
Query will cut off if it is larger than 1K. 
show track_activity_query_size;
Increase this to maybe 32,768 in postgresql.conf and restart postgres.
*/
select now() - query_start as duration,
  datname, query
from pg_stat_activity
where state = 'active'
  and pid <> pg_backend_pid() -- filter out your session
order by 1 desc;

-- figuring out where queries came from
-- ask dev to set application_name variable.

```
  
- Checking for slow queries
  
  Look for individual queries in the log and types of queries that take too much time. 
  show log_min_duration_statement ;
  Sometime, the desired value for above varies from database to database. So set the value at db level. 
```sql
alter database test set log_min_duration_statement to 10000;
```
\d pg_stat_statements - to check query stats

- Inspecting the log
  For DBAs, the following three errors are of great importance.
  - ERROR
  - FATAL - scarier than error
  - PANIC - something is really really wrong
- Checking for missing indexes
```sql
SELECT schemaname, relname, seq_scan, seq_tup_read, 
           idx_scan, seq_tup_read / seq_scan AS avg 
FROM   pg_stat_user_tables 
WHERE  seq_scan > 0 
ORDER BY seq_tup_read DESC 
LIMIT 20;
```
- Checking for memory and I/O

  Consider track_io_timing to ON. 
  - \d pg_stat_database - one way to view I/O stats. Global view of each db. 

    Amount of time PostgreSQL has spent waiting for the OS to respond. Not the disk wait time rather the time taken OS to respond. In many cases, high blk* time occurs when temp_files and temp_bytes show high numbers. Possible reasons could be bad work_mem, maintenance_work_mem settings. 
    - blk_read_time
    - blk_write_time 
  - \d pg_stat_statements 
  
    Calculate the ratio of below two matrics, if it is more than 30% of disk wait can be seen as heavily I/O-bound. 
    - blk_time
    - total_time
  - vmstat - Linux tool. Attention to bi, bo, and wa. 
    - bi - no.of blks read, 1,000 = 1 Mbps
    - bo - blk outs, amount of data written to disk
    - wa - higher the wa value, the slower your queries, bcoz queries are waiting on disk to respond. 
    - 
- Understanding noteworthy errors scenarios
  - clog corruption - it's a total disaster
    - ERROR: could not access status of transaction 118831
  - LOG: checkpoints are occurring too frequently (2 seconds apart)
    - max_wal_size - to increase the checkpoints distnaces
  - Managing corrupted data pages
    - "could not read block %u in file "%s": %m" (appear when blocks can no longer be read)
      - zero_damaged_pages - make this ON (handle it with care)
  - Careless connection management
  - Fighting table bloat
    - \d pg_stat_user_tables


#
# Chapter 13: Migrating to PostgreSQL
 