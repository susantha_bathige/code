
select * from pg_stat_activity

show max_connections;
show config_file;

select * from pg_stat_activity;

cat /etc/passwd
cat /etc/shadow

\q to exit
# send a cmd to the terminal 
\! clear
\pset pager off
\show log_directory
\pset format aligned
# Enable extended display
\x 

in bash

sudo su - postgres
psql

ps -ef | grep postgres

http://postgresguide.com/utilities/psql.html

psql -V
ps auxwww | grep postgres

# login to the server as default user
sudo -i -u postgres psql

createdb mydb
dropdb mydb

# postgres interactive terminal
psql
psql mydb

# use docker image
docker pull postgres:latest

psql -U postgres -c 'SHOW config_file'

sudo -u postgres cat /var/lib/pgsql/12/data/postgresql.conf

# find postgresql service
systemctl list-units | grep postgresql

# updated pg_hba.conf file to allow remote connections

# list the permission list for the current user for sudo. 
sudo -l

# restart postgres service
sudo systemctl status postgresql-12.service

## reload postgres config
SELECT pg_reload_conf();
#or
/usr/bin/pg_ctl reload

psql -h ATI-DEV-PSQL02 -p 6432 -d learnwell -U ehrtutor

# Installing, configuring, pgbouncer
https://www.enterprisedb.com/blog/pgbouncer-tutorial-installing-configuring-and-testing-persistent-postgresql-connection-pooling


## settings
select * from pg_settings;

## check out the init file. 
systemctl cat postgresql-12


### auth methods
<#
trust - anyone who can connect to the server is authorized to access the database

peer - use client's operating system user name as database user name to access it.

md5 - password-base authentication
#>

cat /var/log/efm-3.10/startup-efm.log
less /var/log/efm-3.10/efm.log


# EDB responses

Q1. Proper steps. 

Q2. Share the steps

Q3. solved.

Q4. DB Engine first -> EFM agent next
    if that does not start, then manual steps. 

Q5. shared mount point. 
    streaming repli doc from edb. 

Q6. pg_basebackup - weekly 

Q7. Dedicated VM for witness

Q8. 

Q10. master / standby 
     stop the cluster
     start the efm agent 1 by 1. 

     ALTER SYSTEM 
     include 
     customer preference
#>'

## get postgres port num info
less /etc/services | grep "postgres"

## switch user to enterprisedb -- its a no login user
sudo -u enterprisedb /bin/bash

## pg_dump from a remote server
sudo -u postgres pg_dump --file /srv/pgsql/12/backups/bv_stg --host "ec2-52-1-141-155.compute-1.amazonaws.com" --port "5432" 
--username "ueie8lvjccjuvi" --password --verbose --format=t --blobs --section=data --schema "public" "d1uchr8lktdtps"


./pg_dump --file /srv/pgsql/12/backups/bv_stg --host "ec2-52-1-141-155.compute-1.amazonaws.com" --port "5432"  --username "ueie8lvjccjuvi" --password --verbose --format=t --blobs --section=data --schema "public" "d1uchr8lktdtps"

#usr/local/bin/pg_dump
 ./pg_dump -f /mnt/dump/backup/bv_dump -h "ec2-52-1-141-155.compute-1.amazonaws.com" -p "5432" -U "ueie8lvjccjuvi" -W -v -F=t -b --section=data --schema "public" -d "d1uchr8lktdtps"

psql -h "ec2-52-1-141-155.compute-1.amazonaws.com" -U "ueie8lvjccjuvi" "d1uchr8lktdtps"

## replication broke
https://postgresrocks.enterprisedb.com/t5/EDB-Postgres/Master-and-Slave-Not-Syncing-after-Failover-Test/td-p/2063/

2021-05-18 14:09:49 EDT DETAIL:  End of WAL reached on timeline 3 at 18/F9FFEA70.
cp: cannot stat ‘/pgwal_archive/data/00000004.history’: No such file or directory
cp: cannot stat ‘/pgwal_archive/data/00000005.history’: No such file or directory
cp: cannot stat ‘/pgwal_archive/data/00000004.history’: No such file or directory
2021-05-18 14:09:49 EDT LOG:  new timeline 4 forked off current database system timeline 3 before current recovery point 18/F9FFEAE8


cp: cannot stat ‘/pgwal_archive/data/00000005.history’: No such file or directory
2021-05-18 14:14:14 EDT LOG:  restored log file "00000004.history" from archive
2021-05-18 14:14:14 EDT LOG:  new timeline 4 forked off current database system timeline 3 before current recovery point 18/F9FFEAE8

### How to resetup replication
sudo -u efm $(which efm) stop-cluster efm
# confirm the cluster status
efm cluster-status efm

# log into the standby
sudo -u enterprisedb /bin/bash
# stop postgres

# restore
cd /mnt/dump/backup/
create database d1uchr8lktdtps;
pg_restore -U susanthab --verbose --clean --no-acl --no-owner --dbname d1uchr8lktdtps bv_dump -Fc

pg_restore -U susanthab --verbose --clean --create --data-only --disable-triggers --role ueie8lvjccjuvi --dbname d1uchr8lktdtps --schema public bv_stg.backup -Fc

pg_restore -U ueie8lvjccjuvi --verbose --disable-triggers --role ueie8lvjccjuvi --dbname d1uchr8lktdtps --schema public bv_stg.backup -Fc
# pg_restore: error: options -c/--clean and -a/--data-only cannot be used together
# pwd: sdf7g6SF823jKsd8

# -bash-4.2$ pg_restore -U ueie8lvjccjuvi --verbose --clean --disable-triggers --role ueie8lvjccjuvi --dbname d1uchr8lktdtps --create --schema public bv_stg.backup -Fc
# pg_restore: connecting to database for restore
# Password:
# pg_restore: error: connection to database "d1uchr8lktdtps" failed: FATAL:  database "d1uchr8lktdtps" does not exist
# clean up wal archive manually
sudo -u enterprisedb ./pg_archivecleanup /pgwal_archive/data 0000000300000024000000E1

curl "https://xfrtu.s3.amazonaws.com/bc009797-80f0-4b8a-9704-51e7081ef187/2021-05-28T23%3A44%3A22Z/789d4831-0633-45c4-bfdd-b4bc4104d9bc?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAQKF7VQWOFZPYGBVP%2F20210601%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20210601T140635Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=57b8138845c59f80fc33fd17e59934982b349257dce0ce23b70b73e4a6bb32e5" --output bv_stg.backup



CREATE DATABASE d1uchr8lktdtps
    WITH 
    OWNER = ueie8lvjccjuvi
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

ALTER DATABASE d1uchr8lktdtps
    SET temp_tablespaces TO 'ephemeral';

GRANT ALL ON DATABASE d1uchr8lktdtps TO ueie8lvjccjuvi;

# monitor no.of conflicts
select datname,conflicts from pg_stat_database;
# drill down on conflicts further
select datname , confl_tablespace, confl_lock, confl_snapshot, confl_bufferpin, confl_deadlock 
from pg_stat_database_conflicts;


## checkpoints
```bash
# most recent log file
ls -lt </dir> | head -n 1

# checkpoints
grep checkpoint /db/pgdata/pg_log/postgresql-2019-10-16.log

# 5 longest disk sync
grep 'checkpoint complete:' \
     /db/pgdata/pg_log/postgresql-2019-10-16.log \
     | sed 's/.* sync=/sync=/;' \
     | sed 's/total=[0-9.]* s; //; s/; dist.*//;' \
     | sort -n | tail -n 5

```

# grant all privileges on all tables in schema public to metabase;
# grant all privileges on all sequences in schema public to metaba

# type of errors we need to look for
WARNING
ERROR
LOG
PANIC
grep -E 'ERROR|WARN|PANIC|FATAL/LOG' /var/log/edb/as13/postgresql-Thu.log
grep -E 'PANIC|FATAL|ERROR|WARN' /var/log/edb/as13/postgresql-Thu.log


## recommeneded changes to postgres configs
vacuum_cost_limit = 600 # done
max_connections = 100
checkpoint_completion_target = 0.9 # done
effective_cache_size = 24000MB # done | 0.75 of total RAM 
checkpoint_timeout = 10 min # done
max_wal_size = 10GB # done
autovacuum_naptime = 5 min # done | in sec
log_min_duration_statement = 0 # done
log_autovacuum_min_duration = 0 # done
log_checkpoints = 'on' # done
log_connections = 'on'
log_disconnections = 'on'
log_temp_files = 0


## get pg_settings
select name, setting, unit, context, boot_val, reset_val from pg_settings where name in (
'vacuum_cost_limit','max_connections','checkpoint_completion_target','effective_cache_size','checkpoint_timeout',
'max_wal_size','autovacuum_naptime','log_min_duration_statement','log_autovacuum_min_duration','log_checkpoints','log_connections',
'log_disconnections','log_temp_files');

## create read only user
CREATE USER username WITH PASSWORD 'your_password';
GRANT CONNECT ON DATABASE database_name TO username;
GRANT USAGE ON SCHEMA schema_name TO username;
GRANT SELECT ON ALL TABLES IN SCHEMA schema_name TO username;

## monitor replication lag at standy
SELECT CASE WHEN pg_last_wal_receive_lsn() = pg_last_wal_replay_lsn()
THEN 0
ELSE EXTRACT (EPOCH FROM now() - pg_last_xact_replay_timestamp())
END AS log_delay;

## copy table output to csv
COPY products_273 TO '/tmp/products_199.csv' WITH (FORMAT CSV, HEADER);

## monitoring replication
https://severalnines.com/database-blog/what-look-if-your-postgresql-replication-lagging


## References
https://www.enterprisedb.com/blog/understanding-postgres-parameter-context
# index bloat
https://github.com/pgexperts/pgx_scripts/blob/master/bloat/index_bloat_check.sql

# Calculating checkpoints 

https://www.2ndquadrant.com/en/blog/basics-of-tuning-checkpoints/

# Why Using NFS is bad:

https://www.time-travellers.org/shane/papers/NFS_considered_harmful.html

# Index maintenance:

https://wiki.postgresql.org/wiki/Index_Maintenance

# Disk usage:

https://wiki.postgresql.org/wiki/Disk_Usage

# Information about all sorts of bloat:

https://www.citusdata.com/blog/2017/10/20/monitoring-your-bloat-in-postgres/

# About point of time recovery.

pgbackrest documentation (https://pgbackrest.org/command.html#command-restore) suggests there is a parameter for that, see items 11.1.4. and 11.1.5.


# pg_rewind is a tool for synchronizing a PostgreSQL cluster with another copy of the same cluster, after the clusters' timelines have diverged. 
# A typical scenario is to bring an old master server back online after failover as a standby that follows the new master.
pg_rewind --target-pgdata=/srv/pgsql/13/data --source-server="port=5444 host=10.172.20.83 user=susantha.bathige dbname=d1uchr8lktdtps password=12qwaszx@" --restore-target-wal --dry-run --progress

# bloat
SELECT pg_size_pretty(pg_relation_size('test')) as table_size, pg_size_pretty(pg_relation_size('test_x_idx')) as index_size, (pgstattuple('test')).dead_tuple_percent;

# bloat ratio
SELECT pg_relation_size('test') as table_size, pg_relation_size('test_x_idx') as index_size, 100-(pgstatindex('test_x_idx')).avg_leaf_density as bloat_ratio;


# need to create this extension
CREATE EXTENSION pgstattuple;

# check vacuum and analyse
SELECT
relname AS TableName                                                                       
,n_live_tup AS LiveTuples                                                                   
,n_dead_tup AS DeadTuples                                                                 
,last_autovacuum AS Autovacuum
,last_autoanalyze AS Autoanalyze                                                                    
FROM pg_stat_user_tables where relname in ('active_session');


# autovaccum
select count(*) from pg_stat_activity where backend_type = 'autovacuum worker';

# what autovacuum is doing
select query from pg_stat_activity where backend_type = 'autovacuum worker';

# autovacuum settings
select name, setting, unit from pg_settings where name  ~ 'autovacuum|vacuum.*cost' order by 1;

# references
https://www.depesz.com/2022/03/13/is-my-autovacuum-configured-properly/
https://www.depesz.com/2020/02/18/which-tables-should-be-auto-vacuumed-or-auto-analyzed-update/

# remove index bloat
reindex index concurrently "index_users_weighted_question_scores";

# log all vacuum operations
ALTER SYSTEM SET log_autovacuum_min_duration=0; select pg_reload_conf()


# swap postgres conf files on STG
# put 16cores/64Gb in effect
cd /srv/pgsql/13/data
ln -sf postgresql_custom_16cores64Gb.conf postgresql_custom.conf

# put 8cores/32Gb in effect
ln -sf postgresql_custom_8cores32Gb.conf postgresql_custom.conf



