
select * from pg_stat_activity

show max_connections;
show config_file;

select * from pg_stat_activity;

cat /etc/passwd
cat /etc/shadow

\q to exit
# send a cmd to the terminal 
\! clear
\pset pager off
\show log_directory
\pset format aligned
# Enable extended display
\x 

in bash

sudo su - postgres
psql

ps -ef | grep postgres

http://postgresguide.com/utilities/psql.html

psql -V
ps auxwww | grep postgres

# login to the server as default user
sudo -i -u postgres psql

createdb mydb
dropdb mydb

# postgres interactive terminal
psql
psql mydb

# use docker image
docker pull postgres:latest

psql -U postgres -c 'SHOW config_file'

sudo -u postgres cat /var/lib/pgsql/12/data/postgresql.conf

# find postgresql service
systemctl list-units | grep postgresql

# updated pg_hba.conf file to allow remote connections

# list the permission list for the current user for sudo. 
sudo -l

# restart postgres service
sudo systemctl status postgresql-12.service

## reload postgres config
SELECT pg_reload_conf();
#or
/usr/bin/pg_ctl reload

psql -h ATI-DEV-PSQL02 -p 6432 -d learnwell -U ehrtutor

# Installing, configuring, pgbouncer
https://www.enterprisedb.com/blog/pgbouncer-tutorial-installing-configuring-and-testing-persistent-postgresql-connection-pooling


## settings
select * from pg_settings;

## check out the init file. 
systemctl cat postgresql-12


### auth methods
<#
trust - anyone who can connect to the server is authorized to access the database

peer - use client's operating system user name as database user name to access it.

md5 - password-base authentication
#>

cat /var/log/efm-3.10/startup-efm.log
less /var/log/efm-3.10/efm.log


# EDB responses

Q1. Proper steps. 

Q2. Share the steps

Q3. solved.

Q4. DB Engine first -> EFM agent next
    if that does not start, then manual steps. 

Q5. shared mount point. 
    streaming repli doc from edb. 

Q6. pg_basebackup - weekly 

Q7. Dedicated VM for witness

Q8. 

Q10. master / standby 
     stop the cluster
     start the efm agent 1 by 1. 

     ALTER SYSTEM 
     include 
     customer preference
#>'

## get postgres port num info
less /etc/services | grep "postgres"

## switch user to enterprisedb -- its a no login user
sudo -u enterprisedb /bin/bash

## pg_dump from a remote server
sudo -u postgres pg_dump --file /srv/pgsql/12/backups/bv_stg --host "ec2-52-1-141-155.compute-1.amazonaws.com" --port "5432" 
--username "ueie8lvjccjuvi" --password --verbose --format=t --blobs --section=data --schema "public" "d1uchr8lktdtps"


./pg_dump --file /srv/pgsql/12/backups/bv_stg --host "ec2-52-1-141-155.compute-1.amazonaws.com" --port "5432"  --username "ueie8lvjccjuvi" --password --verbose --format=t --blobs --section=data --schema "public" "d1uchr8lktdtps"

#usr/local/bin/pg_dump
 ./pg_dump -f /mnt/dump/backup/bv_dump -h "ec2-52-1-141-155.compute-1.amazonaws.com" -p "5432" -U "ueie8lvjccjuvi" -W -v -F=t -b --section=data --schema "public" -d "d1uchr8lktdtps"

psql -h "ec2-52-1-141-155.compute-1.amazonaws.com" -U "ueie8lvjccjuvi" "d1uchr8lktdtps"

## replication broke
https://postgresrocks.enterprisedb.com/t5/EDB-Postgres/Master-and-Slave-Not-Syncing-after-Failover-Test/td-p/2063/

2021-05-18 14:09:49 EDT DETAIL:  End of WAL reached on timeline 3 at 18/F9FFEA70.
cp: cannot stat ‘/pgwal_archive/data/00000004.history’: No such file or directory
cp: cannot stat ‘/pgwal_archive/data/00000005.history’: No such file or directory
cp: cannot stat ‘/pgwal_archive/data/00000004.history’: No such file or directory
2021-05-18 14:09:49 EDT LOG:  new timeline 4 forked off current database system timeline 3 before current recovery point 18/F9FFEAE8


cp: cannot stat ‘/pgwal_archive/data/00000005.history’: No such file or directory
2021-05-18 14:14:14 EDT LOG:  restored log file "00000004.history" from archive
2021-05-18 14:14:14 EDT LOG:  new timeline 4 forked off current database system timeline 3 before current recovery point 18/F9FFEAE8

### How to resetup replication
sudo -u efm $(which efm) stop-cluster efm
# confirm the cluster status
efm cluster-status efm

# log into the standby
sudo -u enterprisedb /bin/bash
# stop postgres

# restore
cd /mnt/dump/backup/
create database d1uchr8lktdtps;
pg_restore -U susanthab --verbose --clean --no-acl --no-owner --dbname d1uchr8lktdtps bv_dump -Fc

pg_restore -U susanthab --verbose --clean --create --data-only --disable-triggers --role ueie8lvjccjuvi --dbname d1uchr8lktdtps --schema public bv_stg.backup -Fc

pg_restore -U ueie8lvjccjuvi --verbose --disable-triggers --role ueie8lvjccjuvi --dbname d1uchr8lktdtps --schema public bv_stg.backup -Fc
# pg_restore: error: options -c/--clean and -a/--data-only cannot be used together
# pwd: sdf7g6SF823jKsd8

# -bash-4.2$ pg_restore -U ueie8lvjccjuvi --verbose --clean --disable-triggers --role ueie8lvjccjuvi --dbname d1uchr8lktdtps --create --schema public bv_stg.backup -Fc
# pg_restore: connecting to database for restore
# Password:
# pg_restore: error: connection to database "d1uchr8lktdtps" failed: FATAL:  database "d1uchr8lktdtps" does not exist
# clean up wal archive manually
sudo -u enterprisedb ./pg_archivecleanup /pgwal_archive/data 0000000300000024000000E1

curl "https://xfrtu.s3.amazonaws.com/bc009797-80f0-4b8a-9704-51e7081ef187/2021-05-28T23%3A44%3A22Z/789d4831-0633-45c4-bfdd-b4bc4104d9bc?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAQKF7VQWOFZPYGBVP%2F20210601%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20210601T140635Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=57b8138845c59f80fc33fd17e59934982b349257dce0ce23b70b73e4a6bb32e5" --output bv_stg.backup



CREATE DATABASE d1uchr8lktdtps
    WITH 
    OWNER = ueie8lvjccjuvi
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

ALTER DATABASE d1uchr8lktdtps
    SET temp_tablespaces TO 'ephemeral';

GRANT ALL ON DATABASE d1uchr8lktdtps TO ueie8lvjccjuvi;

# monitor no.of conflicts
select datname,conflicts from pg_stat_database;
# drill down on conflicts further
select datname , confl_tablespace, confl_lock, confl_snapshot, confl_bufferpin, confl_deadlock 
from pg_stat_database_conflicts;


# grant all privileges on all tables in schema public to metabase;
# grant all privileges on all sequences in schema public to metaba
