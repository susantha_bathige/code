## Postgres troubleshooting queries. 

## to see a table is bloated or not. 
SELECT n_live_tup, n_dead_tup from pg_stat_user_tables where relname = 'activity';

## What is currently running
\d pg_stat_activity;


## Execution stats
select query, calls, total_time, min_time, max_time, mean_time, stddev_time, rows from public.pg_stat_statements order by calls desc LIMIT 10;

## Since how much time the query is running
SELECT max(now() - xact_start) FROM pg_stat_activity
        WHERE state IN ('idle in transaction', 'active');
SELECT pid, xact_start FROM pg_stat_activity ORDER BY xact_start ASC;
# if a query is running longer, purhaps for hours, then most likey an issue with that query. 
# so kill the pid.
# To kill use the command below;
SELECT pg_cancel_backend(15681);

## total num of conns
SELECT count(*) FROM pg_stat_activity;

## Num of conns by state
SELECT state, count(*) FROM pg_stat_activity GROUP BY state;

## Connections waiting for a lock
SELECT count(distinct pid) FROM pg_locks WHERE granted = false

## How do I enable query logging using Amazon RDS for PostgreSQL?
https://aws.amazon.com/premiumsupport/knowledge-center/rds-postgresql-query-logging/

## -----------------------
## VACUUM related queries
## -----------------------
https://www.shubhamdipt.com/blog/how-to-clean-dead-tuples-and-monitor-postgresql-using-vacuum/

# VACUUM manually all tables
VACUUM (VERBOSE, ANALYZE);

# tracking dead tuples
SELECT relname, n_dead_tup FROM pg_stat_user_tables;

# Space usage by each table
SELECT relname AS "table_name", pg_size_pretty(pg_table_size(pgc.oid)) AS "space_used" FROM pg_class AS pgc LEFT JOIN pg_namespace AS pgns ON (pgns.oid = pgc.relnamespace) WHERE nspname NOT IN ('pg_catalog', 'information_schema') AND nspname !~ '^pg_toast' AND relkind IN ('r') ORDER BY pg_table_size(pgc.oid) DESC LIMIT 1;

# Autovacuum executed last time
SELECT relname, last_vacuum, last_autovacuum FROM pg_stat_user_tables;

# VACUUM only 1 table
vacuum (analyse,verbose)  activity;

# You can also disable autovacuum at table level
alter table <table name> with (autovacuum = off)

# Check the table size
SELECT pg_size_pretty( pg_relation_size(' t_test'));

# Off autovacuum at table level
alter table activity_deleted_log set ( autovacuum_enabled = off);

## --------------------END of VACUUM related queries----------------------------

## physical position of the row in a table
select ctid, * from t_test order by ctid desc;

## 
select current_database();
select current_user;
select inet_server_addr(),inet_server_port();
select version();
select current_time;

# non interactive mode
psql -f examples.sql
psql -c "SELECT current_time" –f examples.sql -c "SELECT current_time"
\? or \h for help

/* */ -- multiple line comments
\timing

# setting pwd
SET password_encryption = 'scram-sha-256';
\password
alter user myuser password 'secret'

# db version (client version)
psql --version

# server uptime
select date_trunc('second',current_timestamp-pg_postmaster_start_time()) as uptime;



## psql meta-data commands
\conninfo

## search path related troubleshooting
list all the avaibale schemas
select nspname from pg_catalog.pg_namespace;

alter role ueie8lvjccjuvi in database d1uchr8lktdtps set search_path="$user", public, sys;

ALTER ROLE myrole in DATABASE mydb RESET search_path;

## check multiple services
systemctl -t service --all | grep "efm\|edb-as-\|pgbouncer"



