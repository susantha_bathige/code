
## psql 
select current_database();
select current_user;
select inet_server_addr(),inet_server_port();
select version();
select current_time;

## psql metadata commands
\conninfo

psql -c "select current_time"
psql -f examples.sql

# help
\? or \h

# what is the DB server version
select version();
cat $PGDATA/PG_VERSION

# Uptime
SELECT date_trunc('second', current_timestamp - pg_postmaster_start_time()) as uptime;

# default data /log directory on RHEL
/var/lib/pgsql/data
/var/lib/pgsql/12/data
/var/lib/pgsql/data/pg_log

# Postgres bin dir
/usr/pgsql-12/bin/

# how to find database system identifier
sudo -i -u postgres /usr/pgsql-12/bin/pg_controldata /var/lib/pgsql/12/data
sudo -i -u postgres /usr/pgsql-12/bin/pg_controldata /var/lib/pgsql/12/data | grep "system identifier"

# list of databases
\l
select datname from pg_database;

# list of tables
SELECT count(*) FROM information_schema.tables
WHERE table_schema NOT IN ('information_schema','pg_catalog');

# database size
select pg_size_pretty(pg_database_size(current_database()));

# table size
select pg_relation_size('actor');
# table size with indexes
select pg_total_relation_size('actor');
select pg_size_pretty(pg_total_relation_size('actor'));
\dt+ actor

# top 10 biggest tables
SELECT table_name,pg_relation_size(table_schema || '.' || table_name) as size, 
    pg_size_pretty(pg_relation_size(table_schema || '.' || table_name)) as size2
FROM information_schema.tables
WHERE table_schema NOT IN ('information_schema', 'pg_catalog')
ORDER BY size DESC
LIMIT 10;

# estimated no.of rows
SELECT (CASE WHEN reltuples > 0 THEN pg_relation_size(oid)*reltuples/(8192*relpages) 
ELSE 0
END)::bigint AS estimated_row_count
FROM pg_class
WHERE oid = 'actor'::regclass;

# list of extensions
SELECT * FROM pg_extension;
\dx

# list of constrains
\d+ orders

SELECT * FROM pg_constraint
WHERE confrelid = 'orders'::regclass;

# changing configs at session level
SET work_mem = '16MB';
RESET work_mem;
RESET All;

select name, setting, reset_val, source from pg_settings where source = 'session';

# change settings at the transaction level
set local work_mem = '32MB';

# actual location of the config file
show config_file;
show hba_file;
show ident_file;

# non-default settings
SELECT name, source, setting 
                        FROM pg_settings  
                        WHERE source != 'default' 
                        AND source != 'override' 
                        ORDER by 2, 1;

# re-load config files
pg_ctl restart

# change the values of parameter files directly
alter system set shared_buffers = '1GB';

# list all the extensions
select * from pg_available_extensions;

# all the objects in the extension
\dx+ plpgsql

# reload configuration
select pg_reload_conf();

# restricting the connnection to db to 0
alter database  foo_db connection limit 0;

# restrict users to 0 limit
alter user foo connnection limit 0;
# change the pg_hba.conf to refuse connection and reload config
pg_hba_lockdown.conf
pg_hba_access.conf
select * from pg_hba_file_rules;
select * from pg_hba_file_rules \g /tmp/x
cat /tmp/x | perl -pe 's/^\s*\d+\s*//' | sort | uniq -cd

# allow a user connection only one time. 
ALTER ROLE fred CONNECTION LIMIT 1;
# you can eliminate the restriction by setting the value to -1. 
select rolconnlimit from pg_roles;

# create user interactively
createuser --interactive alice

# terminate a user sessions
SELECT pg_terminate_backend(pid)
FROM pg_stat_activity
WHERE ...

SELECT count(pg_terminate_backend(pid))
FROM pg_stat_activity
WHERE usename NOT IN
(SELECT usename FROM pg_user
WHERE usesuper);
# other useful filters are;
WHERE application_name = 'myappname'
WHERE wait_event_type IS NOT NULL AND wait_event_type != 'Activity'
WHERE state = 'idle in transaction'
WHERE state = 'idle'

# create user with superuser
create user susanthab with superuser;
# revoke superuser
alter user with nosuperuser;
# revoke a role from a user
revoke role3 from user2;

# revoke all permissions to a table
revoke all pn table1 from user2;

# about the user
\du susanthab

# display previllages
\dp
\z

# sample permission commands
GRANT SELECT ON table1 TO webreaders; 
GRANT SELECT, INSERT, UPDATE, DELETE ON table1 TO editors; 
GRANT ALL ON table1 TO admins;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO boardvitals;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO boardvitals;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO boardvitals;
GRANT ALL PRIVILEGES ON ALL PROCEDURES IN SCHEMA public TO boardvitals;
GRANT ALL PRIVILEGES ON DATABASE "d1uchr8lktdtps" to boardvitals;


# effective serach path of the current db
show search_path;

# find out the tables affected if you moit the scheman name
\d x

# revoke all from public
REVOKE ALL ON SCHEMA someschema FROM PUBLIC;

# non-ecrypted passwords
select usename,passwd from pg_shadow where passwd
not like ‘SCRAM%’ or passwd is null;

# Giving limitted powers to non-superusers
alter role bob with CreateDB;
alter role bob with CreateRole;

# permission on functions
revoke all on function copy_from( text,  text) from public; 
grant execute on function copy_from( text,  text) to bob;

## Auditing
# logs SQL
log_statement parameter (ALL, MOD, ddl, NONE)
MOD -> DML statements

log_line_prefix %t %u %d

# pg_restore
pg_restore -U susanthab --verbose --clean --no-acl --no-owner --dbname d1uchr8lktdtps bv_dump -Fc

# download a file 
curl http://example.com --output dump

# script 
psql -1 -f myscript.sql
psql --single-transaction -f myscript.sql
psql -f test.sql -v ON_ERROR_STOP=on
# you can also place some sql command in a profile. 

# Variables in the script
# vattest.sql
\set tabname mytable
\set colname mycol
\set colval 'myval'
ALTER TABLE :tabname ADD COLUMN :colname text;
UPDATE :tabname SET :colname = :'colval';
# we can also execute the same script as:
psql -v tabname=mytab2 -f vartest.sql

# tables with largest no.of dead rows
SELECT schemaname
, relname
, n_dead_tup
, n_live_tup
FROM pg_stat_user_tables
ORDER BY n_dead_tup DESC
LIMIT 1;

# see wether the a user is connected
select datname from pg_stat_activity where usename = 'ueie8lvjccjuvi';

# executes a query in every 5 seconds
select count(*) from pg_stat_activity;
\watch 5

# which query is currently running
select datname, usename, state, application_name, wait_event 
from pg_stat_activity
where state not in ('idle')
and usename not in ('susanthab','replicator_stg');
# only active queries
select datname, usename, state, query 
from pg_stat_activity 
where state='active';

#activity of a specific pid
select pg_stat_get_activity(2134);

# longest running query
SELECT
 current_timestamp - query_start AS runtime,
 datname, usename, query
FROM pg_stat_activity
WHERE state = 'active'
ORDER BY 1 DESC;

# queries running more than 1 min
SELECT
    current_timestamp - query_start AS runtime,
    datname, usename, query
FROM pg_stat_activity
WHERE state = 'active'
      AND current_timestamp - query_start > '1 min'
ORDER BY 1 DESC;

# checking which queries are active and blocked
SELECT datname, usename, wait_event_type, wait_event, backend_type, query
FROM pg_stat_activity
WHERE wait_event_type IS NOT NULL
AND wait_event_type NOT IN ('Activity', 'Client');

# blocking pids
pg_blocking_pids()

#sugically terminate an offending session;
select pg_terminate_backend(18399);

# try to cancel the query first before kills
select pg_cancel_backend();
# console cmd - OS
kill -SIGINT <backend_pid>

# what if that does not terminate the pid
kill -9 <pid>
# make sure synchronous_commit -> off before do this kill
# this will be last resort. This will restart the cluster

# terminate a query after x secs 
set statement_timeout to '3 s';

# terminate all queries that are not doing anything in last 10 min
SELECT pg_terminate_backend(pid)
  FROM pg_stat_activity
WHERE state = 'idle in transaction'
   AND current_timestamp - query_start > '10 min';

# identify locks that are no PIDs associated with them
# remove prepared transactions
SELECT t.schemaname || '.' || t.relname AS tablename,
       l.pid, l.granted
       FROM pg_locks l JOIN pg_stat_user_tables t
       ON l.relation = t.relid;

# knowing that anybody is using a specific table

# reset aggregated data
SELECT pg_stat_reset();

# knowing when a table is last used

# using of disk space by temp data
select current_setting('temp_tablespaces');

WITH temporary_tablespaces AS (SELECT 
 unnest(string_to_array(
  current_setting('temp_tablespaces'), ',')
 ) AS temp_tablespace
)  
SELECT tt.temp_tablespace,
pg_tablespace_location(t.oid) AS location,
pg_tablespace_size(t.oid) AS size
FROM temporary_tablespaces tt
JOIN pg_tablespace t ON t.spcname = tt.temp_tablespace
 ORDER BY 1;

# if the temp_tablespaces are empty then check this
SELECT current_setting('data_directory') || '/base/pgsql_tmp';

# find out temp files that are not used any more
SELECT datname, temp_files, pg_size_pretty(temp_bytes) as temp_bytes, stats_reset
  FROM pg_stat_database;
# log_temp_files -> check this parameter
# if you notice increase of temp file creation, you shoudl consider increasing work_mem 
# at least at session level

# why queries are slow
# this will update the stats for the entire db
analyse

# table and index bloat
# check table bloat
SELECT pg_relation_size(relid) AS tablesize,schemaname,relname,n_live_tup
FROM pg_stat_user_tables
WHERE relname = <tablename>;
# see whether table size to n_live_tup make sense
# you can run the analyze on the table and run the query again

# log rotation
show log_rotation_age;

# find out errors
egrep "FATAL|ERROR" /var/log/postgres.log

# analysing real time performance of queries
# most frequent queries
seletc query from pg_stat_statements order by calls desc

# highest avg exec time
SELECT query, total_time/calls AS avg, calls
       FROM pg_stat_statements ORDER BY 2 DESC;

SELECT query, calls FROM pg_stat_statements;

# disable autovacuum for a table
ALTER TABLE big_table SET (autovacuum_enabled = off);

# disable autovacuum for toast table for heavily updates tables
# The Oversized-Attribute Storage Technique
ALTER TABLE pgbench_accounts
SET ( toast.autovacuum_enabled = off);

# display the reloptions and their toat tables
SELECT n.nspname
, c.relname
, array_to_string(
    c.reloptions ||
ARRAY(
SELECT 'toast.' || x
FROM unnest(tc.reloptions) AS x
), ', ')
AS relopts
FROM pg_class c
LEFT JOIN pg_class tc    ON c.reltoastrelid = tc.oid
JOIN pg_namespace n ON c.relnamespace  = n.oid
WHERE c.relkind = 'r'
AND nspname NOT IN ('pg_catalog', 'information_schema');

# to shrink a table
# just vacuum only does not shrink a table
VACUUM FULL

# have autovacuum at different days
# use autovacuum.conf.day
# autovacuum.conf.night

# scan currupted pages
VACUUM (DISABLE_PAGE_SKIPPING);

# check prepared transactions
show max_prepared_transactions;
SELECT * FROM pg_prepared_xacts;

# rows that are affected by a transaction
SELECT * FROM table WHERE xmax = 121083;

# actions for heavy users of temp tables
# vacuum the system tables manually
pg_class, pg_type, pg_attribute

# list of tables to vacuum
SELECT relname, pg_relation_size(oid) FROM pg_class
WHERE relkind in ('i','r') AND relnamespace = 'pg_catalog'::regnamespace
ORDER BY 2 DESC;

# consider changing these parameters
log_autovacuum_min_duration -> logs when this value exceeds
idle_in_transaction_session_timeout -> cancel idle transactions
old_snapshot_threshold

# vacuum needed or not
CREATE OR REPLACE VIEW av_needed AS 
SELECT N.nspname, C.relname 
, pg_stat_get_tuples_inserted(C.oid) AS n_tup_ins 
, pg_stat_get_tuples_updated(C.oid) AS n_tup_upd 
, pg_stat_get_tuples_deleted(C.oid) AS n_tup_del 
, CASE WHEN pg_stat_get_tuples_updated(C.oid) > 0 
       THEN pg_stat_get_tuples_hot_updated(C.oid)::real 
          / pg_stat_get_tuples_updated(C.oid) 
       END 
  AS HOT_update_ratio 
, pg_stat_get_live_tuples(C.oid) AS n_live_tup 
, pg_stat_get_dead_tuples(C.oid) AS n_dead_tup 
, C.reltuples AS reltuples 
, round(COALESCE(threshold.custom, current_setting('autovacuum_vacuum_threshold'))::integer 
        + COALESCE(scale_factor.custom, current_setting('autovacuum_vacuum_scale_factor'))::numeric
       * C.reltuples) 
  AS av_threshold 
, date_trunc('minute', 
    greatest(pg_stat_get_last_vacuum_time(C.oid),
pg_stat_get_last_autovacuum_time(C.oid))) 
  AS last_vacuum 
, date_trunc('minute', 
    greatest(pg_stat_get_last_analyze_time(C.oid), 
             pg_stat_get_last_analyze_time(C.oid))) 
  AS last_analyze 
, pg_stat_get_dead_tuples(C.oid) > 
  round( current_setting('autovacuum_vacuum_threshold')::integer 
       + current_setting('autovacuum_vacuum_scale_factor')::numeric 
       * C.reltuples) 
  AS av_needed 
, CASE WHEN reltuples > 0 
       THEN round(100.0 * pg_stat_get_dead_tuples(C.oid) / reltuples) 
       ELSE 0 END 
  AS pct_dead 
FROM pg_class C 
LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace) 
NATURAL LEFT JOIN LATERAL (
   SELECT (regexp_match(unnest,'^[^=]+=(.+)$'))[1]
   FROM unnest(reloptions)
   WHERE unnest ~ '^autovacuum_vacuum_threshold='
 ) AS threshold(custom)
 NATURAL LEFT JOIN LATERAL (
   SELECT (regexp_match(unnest,'^[^=]+=(.+)$'))[1]
   FROM unnest(reloptions)
   WHERE unnest ~ '^autovacuum_vacuum_scale_factor='
 ) AS scale_factor(custom)
WHERE C.relkind IN ('r', 't', 'm') 
  AND N.nspname NOT IN ('pg_catalog', 'information_schema') 
  AND N.nspname NOT LIKE 'pg_toast%' 
ORDER BY av_needed DESC, n_dead_tup DESC;

\x
SELECT * FROM av_needed WHERE nspname = 'public' AND relname = 'subjects';

# index size and how it changes over time
SELECT
nspname,relname,
round(100 * pg_relation_size(indexrelid) /
                    pg_relation_size(indrelid)) / 100
                AS index_ratio,     
  pg_size_pretty(pg_relation_size(indexrelid))
                AS index_size,
  pg_size_pretty(pg_relation_size(indrelid))
                AS table_size
FROM pg_index I
LEFT JOIN pg_class C ON (C.oid = I.indexrelid)
LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
WHERE
  nspname NOT IN ('pg_catalog', 'information_schema', 'pg_toast') AND
  C.relkind='i' AND 
  pg_relation_size(indrelid) > 0;
# scans the table for more info
SELECT * FROM pgstattuple('pg_catalog.pg_proc');
# much faster and still acurate
select * from pgstattuple_approx('pgbench_accounts');

# progres of the vacuum
# identify the pid of the vacuum process
SELECT * FROM pg_stat_progress_vacuum WHERE pid = 34399;
select * from pg_stat_activity;

# run vacuum job in paralell
vacuumdb --jobs=4 --all

# Maintaining indexes from the OS
reindexdb
# reindex all the dbs
reindex -a
# this takes long time to run
# so instead of that you can follow the below one:
DROP  TABLE  IF  EXISTS  test; CREATE  TABLE  test
(id  INTEGER  PRIMARY  KEY
,category  TEXT
,  value  TEXT);
CREATE  INDEX  ON  test  (category);

SELECT oid, relname, relfilenode
        FROM pg_class
        WHERE oid in (SELECT indexrelid
                      FROM pg_index
                      WHERE indrelid = 'test'::regclass);

# create index concurrently
CREATE INDEX CONCURRENTLY

# unused indexes
select schemaname, relname, indexrelname, idx_scan FROM pg_stat_user_indexes ORDER BY idx_scan;
SELECT schemaname
 , relname
 , indexrelname
 , idx_scan
 FROM pg_stat_user_indexes i
 LEFT JOIN pg_constraint c
   ON i.indexrelid = c.conindid
 WHERE c.contype IS NULL;
# idx_scan is 0 means not used. 

# how to deactivate an index
# list all invalid indexes if any
SELECT ir.relname AS indexname 
, it.relname AS tablename 
, n.nspname AS schemaname 
FROM pg_index i 
JOIN pg_class ir ON ir.oid = i.indexrelid 
JOIN pg_class it ON it.oid = i.indrelid 
JOIN pg_namespace n ON n.oid = it.relnamespace 
WHERE NOT i.indisvalid;
# change valid to invalid

# troubleshooting slow running queries
\dx pg_stat_statements
# top 10 highest workloads of the system
SELECT calls, total_time, query FROM pg_stat_statements 
ORDER BY total_time DESC LIMIT 10;

# if you want to log queries which takes more than 10 sec
ALTER SYSTEM
          SET log_min_duration_statement = 10000;
# reload postgres

# seq scans
pg_stat_user_tables -> seq_tup_scan 
# lots of seq_tup_scan means there are lots of seq scans

# IO performance
pg_statio_user_tables
# look for heap_blks_hit (from buffers) and heap_blks_read (from disk)
pg_statio_user_indexes

# work_mem
# for the queries which uses large sorts and joins
# try setting the work_mem as below
SET work_mem = '1TB';
# run EXPLAIN for the query
# if its getting better then set
RESET work_mem;

# backups and recovery
# if the server crashes there will be msg in log
PANIC

# you should plan your recovery, not your backup. 
# logical backup using pg_dump. The file format is very important. 
pg_dump -F c pgbench > dumpfile
pg_dump -F c -f dumpfile pgbench

# restore
pg_restore --schema-only -v dumpfile 2>/dev/null | head | grep Started

# archive
archive_mode = on 
archive_command = 'rsync -a %p $DRNODE:/archive/%f'
# check the status for archive
select * from pg_stat_archiver;

# enable checksum for backups
data_checksums = on

# replication
# pg_archivecleanup requires directory and %r with space between them. Postgres will transform %r into cut-off 
# file name. 

# look at the current files
ps -ef | grep archiver  on master
ps -ef | grep startup   on standby

# check recovery status
select pg_is_in_recovery();
SELECT pg_is_wal_replay_paused();
SELECT pg_is_in_backup();

# compare two lsn
pg_wal_lsn_diff().

# the current inserted LSN to master
SELECT pg_current_wal_insert_lsn();
# most recent WAL writes at master
SELECT pg_current_wal_lsn();
# most recent recived lsn at standby
SELECT pg_last_wal_receive_lsn();
# the most recent applied LSN on standby
SELECT pg_last_wal_replay_lsn();

# replication delay/lag
SELECT pid, application_name
   ,write_lag, flush_lag, replay_lag
 FROM pg_stat_replication;

# replication status at standby
select * from pg_stat_wal_receiver;

# to pause the replication
SELECT pg_wal_replay_pause();
# to resume
SELECT pg_wal_replay_resume();

# prewarm postgres data
create extension pg_prewarm;
select pg_prewarm('job_status');






























































