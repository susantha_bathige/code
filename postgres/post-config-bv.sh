### =====================================================================================
### On Ascend servers
### =====================================================================================
sudo -u enterprisedb /bin/bash
sudo -iu enterprisedb
cd /srv/pgsql/12/
# default location of the data directory
sudo -u enterprisedb ls /var/lib/edb/as12/data

# verify postgres and pgbouncer are running
# below services should be running and db server should have initialized
systemctl status pgbouncer
systemctl status edb-as-12

# log into postgres
psql -U postgres

# change the auth method to scram in postgresql.conf

create user susanthab with superuser password '12qwaszx@';
create user testuser with password 'testuser';
# copy and past the encrypted pwd of testuser for pgbouncer connection
select usename,passwd from pg_shadow; 
# md5d70a0452418aeb8fb4030eae69ca2856

# create custom postgresql conf file and add the contents below
# take a backup of the data folder in case you need to start from the begining.
cp -r data data_orig
vi data/postgresql_custom.conf
<#
## custom server config
max_connections = 300
shared_buffers = 8GB
effective_cache_size = 24GB
maintenance_work_mem = 2GB
checkpoint_completion_target = 0.9
wal_buffers = '16MB'
default_statistics_target = 100
effective_io_concurrency = 300
random_page_cost = 1.1
work_mem = 6990kB
min_wal_size = 1GB
max_wal_size = 4GB
wal_keep_segments = 32
max_worker_processes = 8
max_parallel_workers_per_gather = 4
max_parallel_workers = 8
max_parallel_maintenance_workers = 4
shared_preload_libraries = 'pg_stat_statements'
data_checksums = on
# password_encryption = scram-sha-256
# log_directory = '/var/log/edb/as12/' -> should be in postgresql.conf
#>
# add postgresql_custom.conf to postgresql.conf
vi data/postgresql.conf
include_if_exists = 'postgresql_custom.conf'

### =====================================================================================
### Setting up pgbouncer
### =====================================================================================

#1. Edit the file below
sudo -u enterprisedb cat > /etc/pgbouncer.d/custom.ini <<EOF
[databases]
postgres = host=127.0.0.1 port=5444 dbname=postgres user=testuser

[pgbouncer]
auth_type = md5
listen_addr = *
listen_port = 6432
auth_file = /etc/pgbouncer/userlist.txt
EOF

#2. Edit the file below
sudo -u pgbouncer cat > /etc/pgbouncer/userlist.txt <<EOF
"testuser" "md5d70a0452418aeb8fb4030eae69ca2856"
EOF

#3. Restart the pgbouncer services
sudo systemctl restart pgbouncer
# check the status
systemctl status pgbouncer

#4. Test the connection
psql -h 127.0.0.1 -p 6432 -U testuser postgres
psql -h 127.0.0.1 -p 6432 -U boardvitals d1uchr8lktdtps

### =====================================================================================
### Setting up replication
### =====================================================================================

https://protect-us.mimecast.com/s/6mVfCDkxBxCM0ZwkhW5PcA?domain=percona.com/

postgresql.conf
listen_addresses = '*'

#In Master
#=========
#1. create the user for repliaction
CREATE USER replicator_stg
  REPLICATION
  LOGIN
  CONNECTION LIMIT 2
  ENCRYPTED PASSWORD 'changeme';
#CREATE USER replicator_stg WITH REPLICATION ENCRYPTED PASSWORD 'GFY%!Xg!`4:!sYWJ';
#2. allow connections from the standby server
sudo -u enterprisedb bash -c 'echo "host replication replicator_stg 10.172.20.68/32 md5" >> /srv/pgsql/12/data/pg_hba.conf'
sudo -u enterprisedb bash -c 'echo "host replication replicator_stg 10.172.20.66/32 md5" >> /srv/pgsql/12/data/pg_hba.conf'
#3. get the changes into effect
psql -U postgres -c "select pg_reload_conf();"

#In Standby (bootstarp the standby server)
#4. stop postgres service
sudo systemctl stop edb-as-12

#5. backup the existing data to data_old
sudo -u enterprisedb /bin/bash
cd /srv/pgsql/12
cp -r data data_orig
rm -r data/*

#6. run pg_basebackup on standby
pg_basebackup -h 10.172.20.66 -U replicator_stg -p 5444 -D /srv/pgsql/12/data -Fp -Xs -P -R

#7. start the postgres service
sudo systemctl start edb-as-12
sudo systemctl status edb-as-12

#8. Verify the replication
# run this on the primary
psql -U postgres -c "select * from pg_stat_replication;"
# alternatively you can run below one as well without login to the postgres
psql -U postgres -x -c "select * from pg_stat_replication;"

#9. ON PRIMARY 
# update archive_mode and archive_command in postgresql_custom.conf as below
echo "archive_mode = on" >> /srv/pgsql/12/data/postgresql_custom.conf
#echo "archive_command = 'cp %p /pgwal_archive/data/%f'" >> /srv/pgsql/12/data/postgresql_custom.conf
echo "archive_command = 'rsync -a %p /pgwal_archive/data/%f'" >> /srv/pgsql/12/data/postgresql_custom.conf
#archive_mode = on
#archive_command = 'cp %p /pgwal_archive/data/%f'
#10. restart the postgres service to config changes to take into effect
sudo systemctl restart edb-as-12

#11. ON STANDBY
# update the restore command
echo "restore_command = 'cp /pgwal_archive/data/%f %p'" >> /srv/pgsql/12/data/postgresql_custom.conf
echo "archive_cleanup_command = '/usr/edb/as12/bin/pg_archivecleanup /pgwal_archive/data %r'" >> /srv/pgsql/12/data/postgresql_custom.conf

#12. restart the postgresql service
sudo systemctl restart edb-as-12

# ON PRIMARY
#13. test replication 
psql -U susanthab postgres
create database testdb;
\c testdb
create table test1 as select Id from generate_series(1, 1000) as Id;
select count(*) from test1;

# ON STANDBY
#14. check the table
psql -U susanthab postgres
\c testdb
select count(*) from test1;
psql -U susnathab postgres -c "select count(*) from test1;"

### =====================================================================================
### Enable ext
### =====================================================================================

# ON PRIMARY/STANDBY
#.1 Create pg_stat_statements ext
# This one is only on primary
create extension pg_stat_statements;

#2. Add the ext to postgresql conf file
#echo "shared_preload_libraries = 'pg_stat_statements'" >> /srv/pgsql/12/data/postgresql_custom.conf

#3. restart postgresql service
sudo systemctl restart edb-as-12

#4. Verify 
psql -U postgres -c "select * from pg_stat_statements;"

# test on both primary and standby
psql -U susanthab postgres -c "select * from pg_stat_statements;"

### =====================================================================================
### Configure EFM - Edb Failover Manager
### =====================================================================================
# ref: https://www.enterprisedb.com/edb-docs/d/edb-postgres-failover-manager/user-guides/user-guide/3.10/index.html

## Prerequisites
# Java/SMTP server/streaming replication

# Create the db user
## Create the db user
# ON MASTER
sudo -u enterprisedb /bin/bash
psql -U postgres
#psql -U postgres -c 'create user efm_bv_stg with password "gE}LFX.7n({xq" login;' -> DID NOT WORK
create user efm_bv_stg with password 'gE}LFX.7n({xq' login;
create database fmdb;
alter database fmdb owner to efm_bv_stg;
# grant required permissions
GRANT pg_read_all_settings TO efm_bv_stg;
\c fmdb grant execute on function pg_wal_replay_resume() to efm_bv_stg;
# otherwise efm agent will not start and get a DB user validation error
grant execute on function pg_last_wal_replay_lsn() to efm_bv_stg;
grant execute on function pg_current_wal_lsn() to efm_bv_stg;
grant execute on function pg_reload_conf() to efm_bv_stg;
grant execute on function pg_wal_replay_resume to efm_bv_stg;

#WARN: User does not have permission to run query pg_wal_replay_resume

# verify the login by connecting 
psql -U efm_bv_stg fmdb

# Modify the pg_hba.conf on master / standby nodes
# ON MASTER
# Note: change IPs accordingly
sudo -u enterprisedb /bin/bash
bash -c 'echo "# access for itself" >> /srv/pgsql/12/data/pg_hba.conf'
bash -c 'echo "host    fmdb         efm_bv_stg      127.0.0.1/32            md5" >> /srv/pgsql/12/data/pg_hba.conf' 
bash -c 'echo "# access for standby" >> /srv/pgsql/12/data/pg_hba.conf'
bash -c 'echo "host    fmdb         efm_bv_stg      10.172.20.68/32         md5" >> /srv/pgsql/12/data/pg_hba.conf' 
bash -c 'echo "# access for witness" >> /srv/pgsql/12/data/pg_hba.conf'
bash -c 'echo "host    fmdb         efm_bv_stg      10.172.20.67/32         md5" >> /srv/pgsql/12/data/pg_hba.conf' 
# reload the config
psql -U postgres -c "select pg_reload_conf();"

# ON STANDBY
# Note: change IPs accordingly
sudo -u enterprisedb /bin/bash
bash -c 'echo "# access for itself" >> /srv/pgsql/12/data/pg_hba.conf'
bash -c 'echo "host    fmdb         efm_bv_stg      127.0.0.1/32            md5" >> /srv/pgsql/12/data/pg_hba.conf' 
bash -c 'echo "# access for standby" >> /srv/pgsql/12/data/pg_hba.conf'
bash -c 'echo "host    fmdb         efm_bv_stg      10.172.20.66/32         md5" >> /srv/pgsql/12/data/pg_hba.conf' 
bash -c 'echo "# access for witness" >> /srv/pgsql/12/data/pg_hba.conf'
bash -c 'echo "host    fmdb         efm_bv_stg      10.172.20.67/32         md5" >> /srv/pgsql/12/data/pg_hba.conf' 
# reload the config
psql -U postgres -c "select pg_reload_conf();"


## Configure efm.properties file
#1. make a copy form the template
sudo -u efm cp /etc/edb/efm-3.10/efm.properties.in /etc/edb/efm-3.10/efm.properties

# encrypt the efm user pwd
# copy the encrypted pwd and paste it in somewhere. 
/usr/edb/efm-3.10/bin/efm encrypt efm

# assign VIP
sudo /usr/edb/efm-3.10/bin/efm_address add4 ens160 10.172.21.252/23
# verify 
ip a
# ping to the IP from all other nodes like below
/bin/ping -q -c3 -w5 10.172.21.252
# release the VIP
sudo /usr/edb/efm-3.10/bin/efm_address del ens160 10.172.21.252/23
# repeat the above process for each node. 

# copy efm.nodes
sudo -u efm cp efm.nodes.in efm.nodes

#. Update the efm.nodes
10.172.20.66:7800
10.172.20.67:7800
10.172.20.68:7800

## specify connection properties in property file
db.user=efm_bv_stg
db.password.encrypted=b4b346f249fb42bc08de1345e5df04d9
db.port=5444
db.database=fmdb
db.service.owner=enterprisedb
db.service.name=edb-as-12.service
db.bin=/usr/edb/as12/bin
user.email=Database.Team@ascendlearning.com
bind.address=10.172.20.66:7800  # IP of the current node
is.witness=false # true on witness node
auto.allow.hosts=true
stable.nodes.file=true
db.data.dir=/srv/pgsql/12/data
from.email=BoardVital_STG@%h.ascendlearning.com

# for VIPs
virtual.ip=10.172.21.252
virtual.ip.interface=ens160
virtual.ip.prefix=23
virtual.ip.single=true

# Add the VIP to pg_bha.conf on both Master and Standby
host    all             all             10.172.21.252/23        md5
# reload the conf file
psql -U postgres -c "select pg_reload_conf()"

#4. Copy the efm.properties and efm.nodes to others nodes
# modify the bind.address and is.witness to true on witness node
sudo -u efm chmod 644 /etc/edb/efm-3.10/efm.properties
sudo -u efm chmod 644 /etc/edb/efm-3.10/efm.nodes

#5. Start the EFM cluster on any node. 
sudo systemctl start edb-efm-3.10

#6. check the status
/usr/edb/efm-3.10/bin/efm cluster-status efm
efm cluster-status efm

#7. Start the efm agent on other nodes and check the status

#8. Check the logs on each node
less /var/log/efm-3.10/startup-efm.log
less /var/log/efm-3.10/efm.log
ls /srv/pgsql/12/data/log/

### =====================================================================================
### Final config of pg_hba.conf
### =====================================================================================

# setup the password for postgres user
alter role postgres with password '{xx6`}!+MVMw{)G#';
# change trust to md5/SCRAM auth

### =====================================================================================
### Failover testing
### =====================================================================================

# Assuming the efm agent is up and running on all the nodes with no issues. 
efm cluster-status efm

# ON MASTER
sudo -u efm $(which efm) promote efm -switchover
# check the cluster-status and verify everything looks good. 
efm cluster-status efm
# ON NEW MASTER
psql -U susanthab postgres
\c testdb
insert into test1 (Id) select Id from generate_series(1,1000) as Id;
select count(*) from test1;
psql -U susanthab test1 -c "select count(*) from test1;"
# check the count on new standby as well and verify it is the same. 
# perform another f/o from new master
# repeat the same process again. 

# IF all above Failover tests are successful, then enable efm-agent on all nodes. 
sudo systemctl enable edb-efm-3.10.service
# check the status and verify
sudo systemctl status edb-efm-3.10.service

### =====================================================================================
### Test pgbouncer with VIP
### =====================================================================================
psql -h 10.172.21.252 -p 6432 -U testuser testdb

### =====================================================================================
### Troubleshooting
### =====================================================================================

# promote
sudo -u efm $(which efm) promote efm -switchover
# stop efm cluster
sudo -u efm $(which efm) stop-cluster efm
# start efm agent
sudo systemctl start edb-efm-3.10
# reload pg conf
psql -U susanthab postgres -c "select pg_reload_conf();"
# stop the server
sudo systemctl stop edb-as-12
sudo systemctl start edb-as-12

# remove VIP
ip a 
sudo /usr/edb/efm-3.10/bin/efm_address del ens160 10.172.21.252/23

# edb postgres bin dir
ls /usr/edb/as12/

# reset pgwal with dry run
sudo systemctl stop edb-as-12
/usr/edb/as12/bin/pg_resetwal -n -f --pgdata /srv/pgsql/12/data
/usr/edb/as12/bin/pg_resetwal -n -f --pgdata /srv/pgsql/12/data -l 000000030000001900000059

### =====================================================================================
### Temp notes
### =====================================================================================

## this should be done in stanby to allow connections from master when HA is configured. 
## because at any time, standby server can become a primary
echo "host replication replicator 192.168.0.107/32 md5" >> $PGDATA/pg_hba.conf

# change the log dir to /var/log/edb/

### =====================================================================================
### References
### =====================================================================================
https://www.enterprisedb.com/postgres-tutorials/postgresql-replication-and-automatic-failover-tutorial
https://www.interdb.jp/pg/pgsql11.html
https://blog.crunchydata.com/blog/how-to-recover-when-postgresql-is-missing-a-wal-file
https://www.postgresql.fastware.com/blog/how-to-solve-the-problem-if-pg_xlog-is-full
https://blog.crunchydata.com/blog/postgres-is-out-of-disk-and-how-to-recover-the-dos-and-donts

### ON master
bash-4.2$ cat /srv/pgsql/12/data/postgresql_custom.conf









