

## How to restore postgres dump from s3:
#1. Download the file from s3. 
curl "<paste the url here>" --output <any backup name>

## check / verify the options before using it. 
## some options may not be applicable and some options may need to add. Check the documentation
pg_restore -U <username> --verbose --role ehrtutor --dbname learnwell --if-exists --clean /tmp/<filename>