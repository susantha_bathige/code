
## Environment
ATI-STG-PSQL04 - Primary
ATI-STG-PSQL05 - Standby
ATI-STG-PSQL06 - Witness

## Components
1. edb advanced server
2. pgbouncer
3. efm 
4. pem agent

## https://www.postgresql.org/docs/13/pgupgrade.html
## Upgrade steps

# pre-requisites
# ==============
# ON PRIMARY NODE
#0. Need more space for the new server's dir because the pg_upgrade will copy the old cluster data
# to the new clusters data dir. 
1. Latest backup
2. Install Postgres 13 and configure
3. No need to start the server
4. Update pg_hba.conf to set the authentication to peer in old cluster
5. stop both servers
    sudo systemctl stop edb-as-12
    sudo systemctl stop edb-as-13
# standby server can remain running until a later step. 
6. Prepare for standby server upgrades
NOTES: TBD

## Upgrade standby
#=================
1. Install the new postgres binaries on the standby server
2. Make sure the standby data dir do not exists or empty
3. Install exetension shared objects as you do it the primary NODE
4. Stop the standby server
    sudo systemctl stop edb-as-12 ???
    sudo systemctl stop edb-as-13

5. Save the below config files because it will be overwritten
   a. postgresql.conf
   b. postgresql.auto.conf
   c. postgresql_custom.conf
   d. pg_hba.conf

6. run rsync
   --dry-run - use dry run option first
NOTE: Need more details on how to run this.

7. Restore pg_hba.conf
   match all the other config files with the old cluster. 

8. Start the new server

9. Post upgrade processing
pg_upgrade will specific if there are any.

## Post upgrade steps
#====================

1. statistics
2. Delete old clusters



# Upgrade
1. run pg_upgrade (new server binary)
   The new binary location would be, "/usr/edb/as12/bin/pg_upgrade"
   --jobs - specify the no.of cores
   --check - to perform only the check
   No one should be accessing the cluster during the upgrades


## Revert to old version (Rollback)
#==================================
