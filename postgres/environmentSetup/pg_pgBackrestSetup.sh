


## check SSH is working from all EDB Advanced server nodes. 
# PSQL01/PSQL02
sudo -u enterprisedb ssh pgbackrest@asc-prd-pbak01
# Its not working from PSQL02. ****

# from backup server, check the followings;
# to all EDB Advanced server nodes. (PSQL01/PSQL02)
sudo -u pgbackrest ssh enterprisedb@ATI-PRD-PSQL01
# SSH not working for PSQL02. 

## =======================================================
## /etc/pgbackrest.conf on backup server (repository host)
## =======================================================



## ========================================
## /etc/pgbackrest.conf on database servers
## ========================================

# pgbackrest.conf
[global]
repo1-host=ASC-PRD-PBAK01
repo1-host-user=pgbackrest
log-level-console=info
log-level-file=debug
delta=y

[demo]
pg1-path=/srv/pgsql/12/data
recovery-option=primary_conninfo=host=primary_vip user=replication_user ...
archive_mode = on
archive_command = 'pgbackrest --stanza=demo /pgwal_archive/data %p'


## From the backup server
# initiate the pgBackRest repository
sudo -u pgbackrest pgbackrest --stanza=demo stanza-create
# check the config and archiving process:
sudo -u pgbackrest pgbackrest --stanza=demo check




## Reference
https://pgstef.github.io/2021/02/02/combining_pgbackrest_with_edb_postgres_advanced_server.html