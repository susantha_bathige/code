
# What to focus
In general query optimization workflow is pretty simple, look at where most of the time is spent (red exclusive column), and then find a way to not do it, do less of it (apply filters earlier, join later, etc.) or do it better (more suitable index)