 SELECT e.id,
    u.first_name AS sender_first_name,
    u.last_name AS sender_last_name,
    e.recipient_list,
    e.cc_list,
    e.bcc_list,
    e.reply_to,
    e.subject,
    e.body,
    e.sent_time,
    e.event_sent,
    e.event_delivered,
    e.event_open,
    e.event_bounce,
    e.event_spamreport
   FROM email e
     LEFT JOIN "user" u ON e.sending_user_id = u.id
  WHERE e.recipient_list !~~ '%root@thebrightlink.com%'::text;


--with NO CAST / ILIKE
SELECT count(view_email.id) AS tbl_row_count
FROM view_email
WHERE 
	view_email.recipient_list ILIKE '%prods%'
	OR view_email.cc_list ILIKE '%prods%'
	OR view_email.bcc_list ILIKE '%prods%'
	OR view_email.subject ILIKE '%prods%'
	OR view_email.body ILIKE '%prods%'

--with NO CAST / LIKE
SELECT count(view_email.id) AS tbl_row_count
FROM view_email
WHERE 
	view_email.recipient_list LIKE '%prods%'
	OR view_email.cc_list LIKE '%prods%'
	OR view_email.bcc_list LIKE '%prods%'
	OR view_email.subject LIKE '%prods%'
	OR view_email.body LIKE '%prods%'

--Original
EXPLAIN SELECT count(view_email.id) AS tbl_row_count
FROM view_email
WHERE CAST(view_email.id AS VARCHAR) ILIKE '%prods%'
	OR CAST(view_email.recipient_list AS VARCHAR) ILIKE '%prods%'
	OR CAST(view_email.cc_list AS VARCHAR) ILIKE '%prods%'
	OR CAST(view_email.bcc_list AS VARCHAR) ILIKE '%prods%'
	OR CAST(view_email.subject AS VARCHAR) ILIKE '%prods%'
	OR CAST(view_email.sent_time AS VARCHAR) ILIKE '%prods%'
	OR CAST(view_email.body AS VARCHAR) ILIKE '%prods%'

--Original with LIKE
EXPLAIN SELECT count(view_email.id) AS tbl_row_count
FROM view_email
WHERE CAST(view_email.id AS VARCHAR) LIKE '%prods%'
	OR CAST(view_email.recipient_list AS VARCHAR) LIKE '%prods%'
	OR CAST(view_email.cc_list AS VARCHAR) LIKE '%prods%'
	OR CAST(view_email.bcc_list AS VARCHAR) LIKE '%prods%'
	OR CAST(view_email.subject AS VARCHAR) LIKE '%prods%'
	OR CAST(view_email.sent_time AS VARCHAR) LIKE '%prods%'
	OR CAST(view_email.body AS VARCHAR) LIKE '%prods%'

---testing
SELECT count(view_email.id) AS tbl_row_count
FROM view_email
WHERE 
	view_email.recipient_list LIKE '%prods%'

SELECT count(view_email.id) AS tbl_row_count
FROM view_email
WHERE 
	   (CAST(view_email.recipient_list AS VARCHAR) ILIKE '%prods%')
    OR CAST(view_email.cc_list AS VARCHAR) LIKE '%prods%'
    OR CAST(view_email.bcc_list AS VARCHAR) LIKE '%prods%'

EXPLAIN SELECT count(view_email.id) AS tbl_row_count
FROM view_email
WHERE 
	CAST(view_email.recipient_list AS VARCHAR) LIKE '%prods%';


CREATE INDEX CONCURRENTLY email_recipient_list_idx_gin ON email USING GIN (recipient_list gin_trgm_ops); 

CREATE INDEX email_cc_list_idx_gin ON email USING GIN (cc gin_trgm_ops);

SELECT pg_size_pretty(pg_total_relation_size('email'));

--===============

-- need to execute this on primary (ATI-PRD-PSQL13)
\c d1uchr8lktdtps
GRANT USAGE ON SCHEMA public TO bv_follower_stg;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO bv_follower_stg;

