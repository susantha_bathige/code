# promote standby to primary
pg_ctl promote

# In primary server
# =================
 
<#
#01. create a role and provide a suitable entry or entries in pg_hba.conf with the database field set to replication
#02. max_wal_senders is set to a sufficiently large value in the configuration file of the primary server
#03. If replication slots will be used, ensure that max_replication_slots is set sufficiently high as well.

# Allow the user "foo" from host 192.168.1.100 to connect to the primary
# as a replication standby if the user's password is correctly supplied.
#
# TYPE  DATABASE        USER                ADDRESS                 METHOD
host    replication     replicator_stg      192.168.1.100/32        md5

# create replication slots
SELECT * FROM pg_create_physical_replication_slot('node_a_slot');

#>


# In Standby server
# =================
#.01. If you plan to have multiple standby servers for high availability purposes, make sure that recovery_target_timeline is set to latest
#.02. If you're setting up the standby server for high availability purposes, set up WAL archiving, connections and authentication like the primary server, because the standby server will work as a primary server after failover.

primary_conninfo = 'host=192.168.1.50 port=5432 user=foo password=foopass options=''-c wal_sender_timeout=5000'''
restore_command = 'cp /path/to/archive/%f %p'
archive_cleanup_command = 'pg_archivecleanup /path/to/archive %r'

# The standby connects to the primary that is running on host 192.168.1.50
# and port 5432 as the user "foo" whose password is "foopass".
primary_conninfo = 'host=192.168.1.50 port=5432 user=foo password=foopass'
primary_slot_name = 'node_a_slot'

# tools
# =====
# pg_archivecleanup

# Monitoring
# ==========
# on primary
select * from pg_stat_replication;
select * from pg_replication_slots;
show wal_level;

# on standby
select * from pg_stat_wal_receiver;
show wal_receiver_status_interval;
show hot_standby;
show hot_standby_feedback;
show primary_slot_name;
select pg_wal_replay_pause();

# on both;
show wal_level; # should be 'replica'
show synchronous_commit;
show archive_mode;
show archive_command;
# to check whether which one is the master
select pg_is_in_recovery();

## How to setup WAL archive
archive_mode = on
archive_command = 'cp %p /archive/%f'

# After setting up wal archive then you can setup a standby server using pg_basebackup like below
pg_basebackup -D /pg_data/database  
    -h localhost
    --checkpoint=fast
    --wal-method-stream # by default it is stream in PG 12. 


## Administration
# check replication
# on master
ps ax | grep sender
# on slave
ps ax | grep receiver

## Avoid instant replication for some cases
# on the standby
# transaction log will still flow from master -> slave: only the replay process is halted. 
select pg_wal_replay_pause()
# resume replay
select pg_wal_replay_resume()

# on master
pg_stat_replication
# on standby
pg_stat_wal_receiver

# Promote pg_ctl or select pg_promote();

## making replication more reliable
# if we do not want the replication slot, we can drop it. 




# replication lag
SELECT client_addr, pg_current_wal_location() - sent_location AS diff FROM pg_stat_replication;
# in milliseconds
SELECT extract(epoch from now() - pg_last_xact_replay_timestamp()) AS replica_lag;

# monitor replication stays up and running at all times. 


# replication status on replica
SELECT status, latest_end_lsn, latest_end_time FROM pg_stat_wal_receiver;

# replication lag
SELECT
  pg_is_in_recovery() AS is_slave,
  pg_last_wal_receive_lsn() AS receive,
  pg_last_wal_replay_lsn() AS replay,
  pg_last_wal_receive_lsn() = pg_last_wal_replay_lsn() AS synced,
  (
   EXTRACT(EPOCH FROM now()) -
   EXTRACT(EPOCH FROM pg_last_xact_replay_timestamp())
  )::int AS lag;

# set async
alter system reset synchronous_standby_names 
##  References
# How to setup replication
https://www.enterprisedb.com/postgres-tutorials/how-manage-replication-and-failover-postgres-version-12-without-recoveryconf




