# help
\h 

# change output format
\pset format csv

\SELECT id, id FROM generate_series( 1, 4) AS id;

# list dbs
\l

# change the db
\c dbname

# Derived columns in SQL Server is known as Generated Column in PostgreSQL

# config file
Postgresql.conf

# NOTE: Understand more about VACUUM. 

# Check the size of a table. 
SELECT pg_size_pretty( pg_relation_size(' t_test'));

# pg_hba.conf (Host based authentication)

# to enable timing of queries
timing

# Set max paralell workers (only for the session)
# to make it permanent, you can set this value in postgresql.conf
SET max_parallel_workers_per_gather TO 0;

# get table size
# pg_relation_size returns the table size in bytes. The below query returns the number of blocks. 
# In SQL Server terms, this is equiallent to the num of pages. 
select pg_relation_size('t_test')/8192.0;

# list of indexes
\di+

# The parameter to control the level of paralellism, this is equal to the MAXDOP in SQL Server
\SHOW max_parallel_maintenance_workers;

# to check the table structure, this is similar to sp_help <table> in sql server.
\d t_test;

# How to enable profile. There is a config in postgresql.conf file. It is in miliiseconds. By default
# it is disable. 
log_min_duration_statement

# Get execution plan with some addtional info. Analyse will not show the plan but it will execute the
# query and show us what has happened. 
# buffers true, will tell us how many 8000 blocks were touched by the query. 
explain (analyse true, buffers true, timing true)

# pg_stats is a system view, containing all the statistics about the contents of the column. 
select tablename, attname, correlation from pg_stats where tablename 
    in ('t_test', 't_random') order by 1,2;

# Index info
\di+
\di+ index_name

# Create index online. In Postgres this is known as conurrently. 
CREATE INDEX CONCURRENTLY idx_name ON t_test (name);

# index types in postgres
select * from pg_am;

# list of extensions 
\dx
select * from pg_extension

# connection info
\conninfo

# connect to psql as a different user
psql -d dbtestsusa -U susantha.bathige

# installed packages
yum list installed | grep postgres

# create user
create user with password '123';

# make a user, superuser
alter role susanthab with superuser;

# load something directly to a table
copy t_location from program 'curl https://www.cybertec-postgresql.com/secret/orte.txt';

# distance between two numbers
# if the number is close to 0 means, the more likely be the strings are similar.
select 'susa' <-> 'susantha';

# Availble extensions
select comment from pg_available_extensions; 

# get max worker threads
show max_worker_processes;

# expanded view enable/disable
\x

#========================
# PostgreSQL system views
#========================

\d pg_stat_activity
select * from pg_stat_activity;

\d pg_stat_database;
select * from pg_stat_database;

# working set
show work_mem;

# table level stats
pg_stat_user_tables
# caching behavior of tables
pg_statio_user_tables

# The idea is to find large tables that have been used frequently in a sequential scan.
SELECT schemaname, relname, seq_scan, seq_tup_read, seq_tup_read / seq_scan AS avg, 
idx_scan FROM pg_stat_user_tables WHERE seq_scan > 0 
ORDER BY seq_tup_read DESC LIMIT 25;

# Analysed index usage. 
pg_stat_user_indexes

SELECT schemaname, relname, indexrelname, idx_scan, pg_size_pretty( pg_relation_size( indexrelid)) AS idx_size, 
pg_size_pretty( sum( pg_relation_size( indexrelid)) OVER (ORDER BY idx_scan, indexrelid)) AS total 
FROM pg_stat_user_indexes
order by 6;

# caching info about indexes. 
pg_statio_user_indexes;

# background writer info
pg_stat_bgwriter;

# which tells us about the archiver process moving the transaction log (WAL) 
# from the main server to some backup device:
select * from pg_stat_archiver;

# restart postgresql
systemctl restart postgresql-12.service

# monitoring archiving process. 

\d pg_stat_archiver

\d pg_stat_replication

\d pg_stat_wal_receiver  # at the receiver end. 

# ssl connection info
\d pg_stat_ssl

# info about your transaction
# this is very much useful for the developers. 
\d pg_stat_xact_user_tables


# check the progress of vacuum operation
\d pg_stat_progress_vacuum

\d pg_stat_progress_create_index

# Info about queries of the system
# NOTE: This has to enable separately. 
\d pg_stat_statements

# Below query is very useful to see what is happening in the system. 
SELECT round(( 100 * total_time / sum( total_time) OVER ()):: numeric, 2) percent, 
round( total_time:: numeric, 2) AS total, calls, round( mean_time:: numeric, 2) AS mean, 
substring( query, 1, 40) FROM pg_stat_statements 
ORDER BY total_time DESC LIMIT 10;

show track_activity_query_size;

SELECT pg_stat_statements_reset();










