

## -------------------------------------------------
## MY LAB
## -------------------------------------------------
# Java install
sudo dnf install java-11-openjdk-devel


efm_cluster_mgr
db.password.encrypted=63ecb2e1d6e19d0c4196c5101f7bc916

/usr/efm-3.10/bin/efm 
stop-cluser efm
cluster-status efm

export PATH=$PATH:/usr/edb/efm-3.10/bin
which efm
efm --version

update properties file and the node file
chown efm:efm efm.nodes.in
chown efm:efm efm.properties.in

# generate the pwd to connect to the db from efm
efm encrypt efm

systectl start efm-3.10

# add new node to the cluster
efm allow-node efm ip

# troubleshooting
cat /var/log/efm-3.10/startup-efm.log
less /var/log/efm-3.10/efm.log (shift+F to go to tail)

## why standby goes to idle even if the server is up and running







