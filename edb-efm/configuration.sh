## Create the db user
create user efm_stg with password 'Z+^r5q[+' login;
create database fmdb;
alter database fmdb owner to efm_bv_stg;
# grant required permissions
GRANT pg_read_all_settings TO efm_stg;
grant execute on function pg_wal_replay_resume() to efm_stg;
grant execute on function pg_last_wal_replay_lsn() to efm_stg;
grant execute on function pg_current_wal_lsn() to efm_stg;
grant execute on function pg_reload_conf() to efm_stg;


# verify whether you can login with the efm_stg user
psql -U efm_stg fmdb

# modify pg_hba.conf file on both master and standby to allow access to each other
# example entry shown below;
# host    fmdb    efm_stg     10.172.20.59/32     md5

/usr/edb/efm-3.10/bin/efm encrypt efm
c634a3de4ce51bb4bcdbad9813d5b373

cat /var/log/efm-3.10/startup-efm.log

sudo -u efm $(which efm) promote efm -switchover
sudo -u efm $(which efm) stop-cluster efm
sudo -u efm $(which efm) cluster-status efm

## Questions to Tim
# Start EFM agent before the DB server to prevent possible issues.

 ls /var/log/edb/as12/ -l

max_connections = '300'
shared_buffers = '2GB'
effective_cache_size = '6GB'
maintenance_work_mem = '512MB'
effective_io_concurrency = '300'
max_worker_processes = '2'
max_parallel_workers_per_gather = '1'
max_parallel_workers = '2'

psql -c "alter system set shared_buffers to '2GB';" postgres
psql -c "alter system set effective_cache_size to '6GB';" postgres
psql -c "alter system set maintenance_work_mem to '512MB';" postgres
psql -c "alter system set effective_io_concurrency to '300';" postgres
psql -c "alter system set max_connections to '300';" postgres
psql -c "alter system set max_worker_processes to '2';" postgres
psql -c "alter system set max_parallel_workers_per_gather to '1';" postgres
psql -c "alter system set max_parallel_workers to '2';" postgres

## Steps
1. Alter system in master
2. reload conf
3. Alter system in standby
4. reload conf
5. restart standby (if successful)
6. Verify cluster-status (if everything okay, go to next step)
7. failover master to standby
8. Verify cluster-status (if everything okay, go to next step)
6. restart master db service
9. Verify cluster-status (if everything okay, go to next step)
10. failover master to standby (previous master)
11. Verify cluster-status (if everything okay, go to next step)
12. End


###============================================
alter system set max_connections to 300;
alter system set shared_buffers to '2GB';
alter system set effective_cache_size to '6GB';
alter system set maintenance_work_mem to '512MB';
alter system set wal_buffers to '16MB';
alter system set effective_io_concurrency to 300;
alter system set work_mem to '3495kB';

alter system set max_worker_processes to 4;

alter system set max_parallel_workers_per_gather to 2;
alter system set max_parallel_workers to 4;
alter system set max_parallel_maintenance_workers to 2;
###============================================

## Did not change
alter system set checkpoint_completion_target = 0.9
alter system set default_statistics_target = 100
alter system set random_page_cost = 1.1
alter system set min_wal_size = 2GB
alter system set max_wal_size = 8GB

## Errors
#When setting this in standby server, set this to the same or higher value on the master
parameter "max_worker_processes" cannot be changed without restarting the server

## Questions to EDB
1. Do we need to set archive_command


## efm service
systemctl status edb-efm-3.10


## setting up VIP

## assign VIP to a node. 
# https://protect-us.mimecast.com/s/s5dnCERyDyt6E1GEUPKHis?domain=enterprisedb.com
sudo /usr/edb/efm-3.10/bin/efm_address add4 ens160 10.172.21.253/23 

# to release VIP
sudo /usr/edb/efm-3.10/bin/efm_address del ens160 10.172.21.253/23

# efm.properties file
# VIP: 10.172.21.253
# all values are mandatory
virtual.ip='atistgpgvip01.ad.ascendlearning.com'
virtual.ip.interface=ens160
virtual.ip.prefix=23
virtual.ip.single=true

## if the primary node taken out of sync rellication by the EFM,
## then use the below command to bring that back.
## on primary
ALTER SYSTEM RESET synchronous_standby_names;
select pg_reload_conf();